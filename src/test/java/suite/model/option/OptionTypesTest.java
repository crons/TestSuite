package suite.model.option;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.stream.Collectors;

import static org.junit.Assert.assertNotNull;

@RunWith(Parameterized.class)
public class OptionTypesTest {

    private OptionTypes type;

    public OptionTypesTest(OptionTypes type) {
        this.type = type;
    }

    @Parameterized.Parameters
    public static Iterable<? extends Object> data() {
        return Arrays.stream(OptionTypes.values())
                .collect(Collectors.toList());
    }

    @Test
    public void testIsCorrect() throws Exception {
        assertNotNull(type.isCorrect());
    }

    @Test
    public void testToString() throws Exception {
        assertNotNull(type.toString());
    }

}