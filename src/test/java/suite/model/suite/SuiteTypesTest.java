package suite.model.suite;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.stream.Collectors;

import static org.junit.Assert.assertNotNull;

@RunWith(Parameterized.class)
public class SuiteTypesTest {

    private SuiteTypes type;

    public SuiteTypesTest(SuiteTypes type) {
        this.type = type;
    }

    @Parameterized.Parameters
    public static Iterable<? extends Object> data() {
        return Arrays.stream(SuiteTypes.values())
                .collect(Collectors.toList());
    }

    @Test
    public void isSurvey() throws Exception {
        assertNotNull(type.isSurvey());
    }

    @Test
    public void isTest() throws Exception {
        assertNotNull(type.isTest());
    }

    @Test
    public void testToString() throws Exception {
        assertNotNull(type.toString());
    }

}