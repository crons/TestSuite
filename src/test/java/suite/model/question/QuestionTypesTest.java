package suite.model.question;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.stream.Collectors;

import static org.junit.Assert.assertNotNull;

@RunWith(Parameterized.class)
public class QuestionTypesTest {

    private QuestionTypes type;

    public QuestionTypesTest(QuestionTypes type) {
        this.type = type;
    }

    @Parameterized.Parameters
    public static Iterable<? extends Object> data() {
        return Arrays.stream(QuestionTypes.values())
                .collect(Collectors.toList());
    }

    @Test
    public void testtoString() throws Exception {
        assertNotNull(type.toString());
    }

}