package suite.util.stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.stream.Collectors;

import static org.junit.Assert.assertNotNull;

@RunWith(Parameterized.class)
public class StreamTypesTest {

    private StreamTypes type;

    public StreamTypesTest(StreamTypes type) {
        this.type = type;
    }

    @Parameterized.Parameters
    public static Iterable<? extends Object> data() {
        return Arrays.stream(StreamTypes.values())
                .collect(Collectors.toList());
    }

    @Test
    public void isConsole() throws Exception {
        assertNotNull(type.isConsole());
    }

    @Test
    public void testToString() throws Exception {
        assertNotNull(type.toString());
    }

}