package suite.model.question;

import suite.model.Option;
import suite.model.Question;
import suite.model.options.EssayOptions;
import suite.util.DependencyException;
import suite.util.LogContext;

import java.util.Arrays;
import java.util.List;

/**
 * <p>
 *     Abstraction to represent the EssayQuestion and the
 *     Short answer type questions
 * </p>
 *
 * @author Prakash
 */
public class EssayQuestion extends DefaultQuestion implements Question, java.io.Serializable {

    private final LogContext LOGGER = new LogContext(EssayQuestion.class);

    /**
     * Default constructor
     */
    public EssayQuestion() {
        super();
    }

    /**
     * Parametrized constructor for bean settings
     * @param type {@link QuestionTypes}
     */
    public EssayQuestion(QuestionTypes type) {
        super(type);
    }

    /**
     * Override for the answer() to print all the answers following
     * the format of the question
     * @return {@link String}
     * @throws DependencyException
     */
    @Override
    public String answer() throws DependencyException {
        if (getType().isEssay()) {
            return "[ESSAY]\n";
        } else {
            return "[CORRECT ANSWERS]\n\n" + getOptions().correct();
        }
    }

    /**
     * Override to set the instance of {@link EssayOptions}
     * @param options {@link Option} stream
     * @return {@link DefaultQuestion}
     * @throws DependencyException In case of operational errors
     */
    @Override
    public DefaultQuestion setOptions(Option... options)
            throws DependencyException {
        return setOptions(new EssayOptions(
                        getType(),
                        Arrays.asList(options)
                )
        );
    }

    /**
     * Throws a {@link DependencyException} because this is not supported
     * @param identifier String identifier
     * @return {@link Option}
     * @throws DependencyException always
     * @deprecated Unsupported operation
     */
    @Deprecated
    @Override
    public Option getOption(String identifier) throws DependencyException {
        LOGGER.addContext("Method", "getOption");
        LOGGER.fatal(new DependencyException());
        throw new DependencyException("Not applicable");
    }

    /**
     * Throws a {@link DependencyException} because this is not supported
     * @return List of string
     * @throws DependencyException always
     * @deprecated Unsupported operation
     */
    @Deprecated
    @Override
    public List<String> getPossibleChoices() throws DependencyException {
        LOGGER.addContext("Method", "getPossibleChoices");
        LOGGER.fatal(new DependencyException());
        throw new DependencyException("Not applicable");
    }

    /**
     * Sets the correct answer for the short answer question.
     * @param options specified {@link Option} stream
     * @return {@link DefaultQuestion}
     * @throws DependencyException In case there are operational errors
     */
    @Override
    public EssayQuestion setCorrectOption(Option... options)
            throws DependencyException {
        LOGGER.addContext("Method", "setCorrectOptions");
       try {
            setOptions(options);
            getOptions().set(options);
            return this;
        } catch (DependencyException e) {
            LOGGER.error(e);
            throw new DependencyException("Option can't be graded", e);
        }
    }

    /**
     * Throws {@link DependencyException} since this operation is unsupported
     * @param identifiers String identifier stream
     * @return {@link DefaultQuestion}
     * @throws DependencyException Unsupported operation so everytime
     * @deprecated Unsupported operation
     */
    @Deprecated
    @Override
    public EssayQuestion setCorrectOption(String... identifiers)
            throws DependencyException {
        LOGGER.addContext("Method", "setCorrectOption");
        LOGGER.fatal(new DependencyException());
        throw new DependencyException("Not applicable");
    }

    /**
     * Override for toString()
     * @return {@link String}
     */
    @Override
    public String toString() {
        return ((getType().isEssay()) ? "[ESSAY]\n" : "[SHORT ANSWER]\n")
                + "[ RESPONSES ] -> " + getNumberOfChoices() + "\n"
                + "Q: " + getPrompt() + "\n";
    }

}
