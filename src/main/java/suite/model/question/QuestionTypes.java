package suite.model.question;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * <p>
 *     Enumeration to hold the different question types <br>
 *         Grading mechanism can be deduced by the <b>multiple</b>
 *         and the <b>graded</b> parameters
 * </p>
 *
 * @author Prakash
 */

public enum QuestionTypes implements java.io.Serializable {

    NONE(false, false, false, false, false, false, false, false, false),
    SURVEY_DEFAULT(true, false, false, false, false, false, false, false, true),
    SURVEY_MULTIPLE(true, false, true, false, false, false, false, false, true),
    TEST_DEFAULT(false, true, false, false, false, false, false, false, false),
    TEST_MULTIPLE(false, true, true, false, false, false, false, false, false),
    SURVEY_RANKING(true, false, true, true, false, false, false, false, true),
    TEST_RANKING(false, true, true, true, false, false, false, false, false),
    SURVEY_MATCHING(true, false, false, false, false, false, true, false, true),
    TEST_MATCHING(false, true, false, false, false, false, true, false, false),
    SURVEY_ESSAY(true, false, false, false, true, false, false, true, true),
    SURVEY_ESSAY_MULTIPLE(true, false, true, false, true, false, false, true, true),
    TEST_ESSAY(false, true, false, false, true, false, false, true, true),
    SURVEY_SHORT(true, false, false, false, false, true, false, true, true),
    TEST_SHORT(false, true, false, false, false, true, false, true, false),
    TEST_ESSAY_MULTIPLE(false, true, true, false, true, false, false, true, true),
    SURVEY_SHORT_MULTIPLE(true, false, true, false, false, true, false, true, true),
    TEST_SHORT_MULTIPLE(false, true, true, false, false, true, false, true, false),
    TEST_TRUE_FALSE(false, true, false, false, false, false, false, true, false),
    SURVEY_TRUE_FALSE(true, false, false, false, false, false, false, true, true)
    ;

    private final boolean survey;
    private final boolean graded;
    private final boolean multiple;
    private final boolean ranking;
    private final boolean essay;
    private final boolean shortAnswer;
    private final boolean matching;
    private final boolean nonModifiableOptions;
    private final boolean nonModifiableAnswers;

    /**
     * Constructor to set the gradable and multiple parameters
     * @param survey Whether the question is test/survey question
     * @param graded Whether the question should be graded
     * @param multiple Whether the question is multiple choice
     * @param ranking Whether its a ranking question
     * @param essay Whether its an essay question
     * @param shortAnswer Whether its a short-answer question
     * @param matching Whether its matching question
     * @param nonModifiableOptions Whether the options are modifiable
     * @param nonModifiableAnswers Whether the answers are non-modifiable
     */
    QuestionTypes(final boolean survey,
                  final boolean graded,
                  final boolean multiple,
                  final boolean ranking,
                  final boolean essay,
                  final boolean shortAnswer,
                  final boolean matching,
                  final boolean nonModifiableOptions,
                  final boolean nonModifiableAnswers) {
        this.survey = survey;
        this.graded = graded;
        this.multiple = multiple;
        this.ranking = ranking;
        this.essay = essay;
        this.shortAnswer = shortAnswer;
        this.matching = matching;
        this.nonModifiableOptions = nonModifiableOptions;
        this.nonModifiableAnswers = nonModifiableAnswers;
    }

    /**
     * Gets whether the question is gradable or not
     * @return boolean
     */
    public boolean isGraded() {
        return graded;
    }

    /**
     * Gets whether the question is multiple-choice
     * @return boolean
     */
    public boolean isMultiple() {
        return multiple;
    }

    /**
     * Gets whether the question is a part of survey or not
     * @return boolean
     */
    public boolean isSurvey() {
        return survey;
    }

    /**
     * Gets whether it is a ranking question
     * @return boolean
     */
    public boolean isRanking() {
        return ranking;
    }

    /**
     * Gets whether the question is a test question
     * @return boolean
     */
    public boolean isTest() {
        return !isSurvey() && this != NONE;
    }

    /**
     * @return Whether the question is of essay type
     */
    public boolean isEssay() {
        return essay;
    }

    /**
     * @return Whether the question is short answer or not
     */
    public boolean isShortAnswer() {
        return shortAnswer;
    }

    /**
     * @return Whether the question is of matching type
     */
    public boolean isMatching() {
        return matching;
    }

    /**
     * @return Whether the question type is true/false
     */
    public boolean isNonModifiableOptions() {
        return nonModifiableOptions;
    }

    /**
     * @return Whether the answers are modifiable
     */
    public boolean isNonModifiableAnswers() {
        return nonModifiableAnswers;
    }

    /**
     * <p>
     *     Override to show the full parameter description
     *     of
     * </p>
     * @return {@link String}
     */
    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(
                this,
                ToStringStyle.MULTI_LINE_STYLE
        );
    }



}
