package suite.model.question;

import suite.model.Option;
import suite.model.Question;
import suite.model.option.RankingOption;
import suite.model.options.RankingOptions;
import suite.util.DependencyException;
import suite.util.LogContext;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *     Abstraction layer to provide the overrides for the ranking
 *     and maybe matching type question
 * </p>
 *
 * @author Prakash
 */
public class RankingQuestion extends DefaultQuestion implements Question, java.io.Serializable {

    private final LogContext LOGGER = new LogContext(RankingQuestion.class);

    /**
     * Default Constructor
     */
    public RankingQuestion() {
        super();
    }

    /**
     * Parametrized constructor to receive the default values
     * from the bean configuration
     * @param type {@link QuestionTypes}
     */
    public RankingQuestion(QuestionTypes type) {
        super(type);
    }

    /**
     * Override for the correct answer to display the rank along
     * with the identifier
     * @return {@link String}
     * @throws DependencyException In case the instance is not of {@link RankingOption}
     */
    @Override
    public String answer() throws DependencyException {
        LOGGER.addContext("Method", "answer");
        LOGGER.addContext("Options", getOptions().toString());
        if (!(getOptions() instanceof RankingOptions)) {
            LOGGER.error(new DependencyException());
            throw new DependencyException("Options are not ranking type");
        }

        String string = "";
        string += "\n[CORRECT ANSWER]\n\n";
        for (Option option: getOptions().correct().options()) {
            if (!(option instanceof RankingOption)) {
                LOGGER.error(new DependencyException());
                throw new DependencyException("Option is not ranking type");
            }
            if (getType().isRanking()) {
                string += option.getIdentifier() + " - " + ((RankingOption) option).getRank() + "\n";
            } else {
                string += option.getOption() + " - " + ((RankingOption) option).getRank() + "\n";
            }
        }
        return string;
    }

    /**
     * Sets the options with the {@link Option} stream
     * @param options {@link Option} stream
     * @return {@link DefaultQuestion}
     * @throws DependencyException
     */
    @Override
    public DefaultQuestion setOptions(Option... options)
            throws DependencyException {
        return setOptions(new RankingOptions(
                getType(),
                Arrays.asList(options)
        ));
    }

    /**
     * Gets a list of possible choices, for the {@link RankingOptions}
     * this is dynamic
     * @return List of {@link String}
     * @throws DependencyException In case of operational errors
     */
    @Override
    public List<String> getPossibleChoices() throws DependencyException {
        LOGGER.addContext("Method", "getPossibleChoices");
        try {
            List<String> alreadyThere = ((RankingOptions) getOptions()).getOptions()
                    .values()
                    .stream()
                    .filter(rank -> !rank.equals("N/A"))
                    .collect(Collectors.toList());

            List<String> possible = new LinkedList<>();
            if (getType().isRanking()) {
                for (Integer i = 1; i <= getOptions().size(); i++) {
                    possible.add(i.toString());
                }
            } else {
                for (Option option: getOptions().options()) {
                    possible.add(option.getIdentifier());
                }
            }

            possible.removeAll(alreadyThere);
            return possible;
        } catch (Exception e) {
            LOGGER.error(e);
            throw new DependencyException("Error while getting possible choices", e);
        }
    }

    /**
     * Gets a list of possible ranking responses
     * @param optionsToIgnore List of {@link Option} to ignore
     * @return list of ranking selections
     */
    @Override
    public List<String> getPossibleSelections(List<Option> optionsToIgnore) {
        List<String> possible = new LinkedList<>();
        if (getType().isRanking()) {
            for (Integer i = 1; i <= getNumberOfChoices(); i++) {
                possible.add(i.toString());
            }
        } else {
            possible.addAll(
                    getOptions().options()
                            .stream()
                            .map(option -> option.getIdentifier())
                            .collect(Collectors.toList())
            );
        }
        possible.removeAll(
                optionsToIgnore
                        .parallelStream()
                        .map(option -> option.getRank())
                        .collect(Collectors.toList())
        );
        return possible;
    }

    /**
     * Override for the toSring()
     * @return {@link String}
     */
    @Override
    public String toString() {
        return ((getType().isMatching()) ? "[MATCHING]\n" : "[RANKING]\n")
                + "Q: " + getPrompt() + "\n\n" + getOptions();
    }

}
