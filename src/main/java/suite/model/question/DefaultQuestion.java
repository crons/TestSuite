package suite.model.question;

import suite.model.Options;
import suite.model.Question;
import suite.model.Option;
import suite.model.options.DefaultOptions;
import suite.util.DependencyException;
import suite.util.LogContext;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *     Abstraction for the default questions handled by the test suite. <br>
 *         <ul>
 *             <li><b>Does not include the essay type answers</b></li>
 *             <li>Questions take a string for prompt and options</li>
 *         </ul>
 * </p>
 *
 * @author Prakash
 */
public class DefaultQuestion implements Question, java.io.Serializable {

    private final LogContext LOGGER = new LogContext(DefaultQuestion.class);

    private QuestionTypes type;
    private String prompt;
    protected Options options;
    private Integer numberOfChoices;

    /**
     * <p>
     *     Default constructor for the Question. <br>
     *         <b>Assumes it is a survey.</b>
     * </p>
     */
    public DefaultQuestion() {
        type = QuestionTypes.NONE;
        options = new DefaultOptions();
        numberOfChoices = 1;
    }

    /**
     * <p>
     *     Parametrized constructor to set the type for the question <br>
     *         <b> Options are not provided in this
     *         constructor, use <code>withOptions()</code>
     *         method</b> <br>
     *             Also, for setting the stream use
     *             <code><b>withPrompt()</b></code> method
     * </p>
     * @param type {@link QuestionTypes} for the question
     */
    public DefaultQuestion(QuestionTypes type) {
        this.type = type;
        this.numberOfChoices = 1;
    }

    /**
     * Sets the {@link QuestionTypes} for the question
     * @param type {@link QuestionTypes}
     * @return {@link DefaultQuestion}
     */
    @Override
    public DefaultQuestion setType(QuestionTypes type) {
        this.type = type;
        if (options != null) {
            options.setType(type);
        }
        return this;
    }

    /**
     * Gets the number of correct options
     * @return {@link Integer}
     */
    @Override
    public Integer getNumberOfChoices() {
        return numberOfChoices;
    }

    /**
     * Sets the number of correct options
     * @param numberOfChoices Number of correct options
     * @return {@link DefaultQuestion}
     */
    @Override
    public DefaultQuestion setNumberOfChoices(Integer numberOfChoices) {
        this.numberOfChoices = numberOfChoices;
        return this;
    }

    /**
     * Gets the prompt for the question
     * @return {@link String}
     */
    @Override
    public String getPrompt() {
        return prompt;
    }

    /**
     * Gets the type of the question
     * @return {@link QuestionTypes}
     */
    @Override
    public QuestionTypes getType() {
        return type;
    }

    /**
     * <p>
     *     Sets the prompt to the given question with the
     *     user input
     * </p>
     * @param prompt {@link String} for the question
     * @return {@link DefaultQuestion}
     */
    @Override
    public DefaultQuestion setPrompt(String prompt) {
        this.prompt = prompt;
        return this;
    }

    /**
     * <p>
     *     Sets the options for the question taking a
     *     {@link Options} object
     * </p>
     * @param options {@link Options} object
     * @return {@link DefaultQuestion}
     */
    @Override
    public DefaultQuestion setOptions(Options options) {
        this.options = options;
        return this;
    }

    /**
     * Gets the correct options
     * @return {@link String}
     * @throws DependencyException In case of operational errors
     */
    @Override
    public String answer() throws DependencyException {
        return ((!type.isMultiple()) ? "\n[CORRECT ANSWER] " :
                "\n[CORRECT ANSWERS] ") +
                options.correct().toString();
    }

    /**
     * <p>
     *     Sets the options for the question with a stream
     *     of {@link Option} objects
     * </p>
     * @param options {@link Option} stream
     * @return {@link DefaultQuestion}
     * @exception DependencyException In case of operational errors
     */
    @Override
    public DefaultQuestion setOptions(Option... options)
            throws DependencyException {
        return setOptions(new DefaultOptions(
                type,
                Arrays.asList(options)
        ));
    }

    /**
     * <p>
     *     Gets the option from the given identifier. The option must exist
     *     with the same identifier in order to be extracted
     * </p>
     * @param identifier String identifier
     * @return {@link Option} with the same identifier
     * @throws DependencyException In case the option is not found
     */
    @Override
    public Option getOption(String identifier) throws DependencyException {
        try {
            return options.options()
                    .stream()
                    .filter(option -> identifier.equalsIgnoreCase(option.getIdentifier()))
                    .findFirst()
                    .get();
        } catch (Exception e) {
            throw new DependencyException("Error while getting option from identifier", e);
        }
    }

    /**
     * Gets the options for a question
     * @return {@link Options}
     */
    @Override
    public Options getOptions() {
        return options;
    }

    /**
     * Gets the possible identifiers for options to handle edge cases
     * @return List of string
     */
    @Override
    public List<String> getPossibleChoices() {
        List<String> choices = options.options()
                .stream()
                .map(Option::getIdentifier)
                .collect(Collectors.toList());
        return choices;
    }

    /**
     * <p>
     *     Sets the correct option to the {@link DefaultQuestion} based
     *     on the specified {@link Option} object
     * </p>
     * @param options specified {@link Option} stream
     * @return {@link DefaultQuestion}
     * @throws DependencyException In case the question can't be graded
     */
    @Override
    public DefaultQuestion setCorrectOption(Option... options)
            throws DependencyException {
        LOGGER.addContext("Method", "setCorrectOption");
        try {
            this.options.set(options);
            return this;
        } catch (DependencyException e) {
            LOGGER.error(e);
            throw new DependencyException("This question can't be graded", e);
        }
    }

    /**
     * <p>
     *     Sets the correct option to the {@link DefaultQuestion} based
     *     on the {@link String} identifier provided
     * </p>
     * @param identifiers String identifier stream
     * @return {@link DefaultQuestion}
     * @throws DependencyException In case the identifier is invalid choice
     */
    @Override
    public DefaultQuestion setCorrectOption(String... identifiers)
            throws DependencyException {
        List<Option> options;
        try {
            options = Arrays.stream(identifiers)
                    .map(identifier -> getOption(identifier))
                    .collect(Collectors.toList());
            setCorrectOption(options.toArray(new Option[options.size()]));
            return this;
        } catch (DependencyException e) {
            throw new DependencyException("Invalid choice selected", e);
        }
    }

    /**
     * <p>
     *     Gets a selection of identifiers after removing the specified list
     *     for streaming purposes
     * </p>
     * @param optionsToIgnore List of {@link Option} to ignore
     * @return List of identifiers
     */
    @Override
    public List<String> getPossibleSelections(List<Option> optionsToIgnore) {
        List<String> identifiers = getPossibleChoices();
        identifiers.removeAll(
                optionsToIgnore
                        .stream()
                        .map(option -> option.getIdentifier())
                        .collect(Collectors.toList())
        );
        return identifiers;
    }

    /**
     * Override for the equals() method
     * @param o {@link Object}
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DefaultQuestion that = (DefaultQuestion) o;

        if (type != that.type) return false;
        if (prompt != null ? !prompt.equals(that.prompt) : that.prompt != null) return false;
        return numberOfChoices != null ? numberOfChoices.equals(that.numberOfChoices) : that.numberOfChoices == null;
    }

    /**
     * Override for the hash function
     * @return hashcode
     */
    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (prompt != null ? prompt.hashCode() : 0);
        result = 31 * result + (numberOfChoices != null ? numberOfChoices.hashCode() : 0);
        return result;
    }

    /**
     * <p>
     *     Override for default toString(). Displays the question formatting: <br>
     *         <code>question \n options</code>
     * </p>
     * @return {@link String}
     */
    @Override
    public String toString() {
        return ((type.isMultiple()) ? "[MULTIPLE CORRECT: " + numberOfChoices + " ]\n" : "[SINGLE CORRECT]\n") +
                "Q: " + prompt + "\n" + options;
    }

}
