package suite.model.menu;

import com.google.common.collect.ImmutableList;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import suite.config.MenuConfig;
import suite.model.Menu;
import suite.util.stream.Stream;
import suite.util.DependencyException;
import suite.util.LogContext;
import suite.util.SafeGet;

import java.util.List;

/**
 * <p>
 *     Class to handle the main menu of the TestSuite.
 *     Goes to the corresponding sub-menu based on user input.
 * </p>
 *
 * @author Prakash
 */
public class MainMenu implements Menu {

    private final LogContext LOGGER = new LogContext(MainMenu.class);
    protected final static ApplicationContext CONTEXT
            = new AnnotationConfigApplicationContext(MenuConfig.class);

    private final String options;
    private final List<String> choices;
    private final Stream stream;
    private Menu parent;

    /**
     * Default Constructor. Sets the Main menu options.
     * @deprecated Uses a deprecated constructor for Stream
     */
    @Deprecated
    public MainMenu() {
        options = "1. Survey\n2. Test\n3. Quit";
        choices = ImmutableList.of("1", "2", "3");
        stream = new Stream();
        parent = null;
    }

    /**
     * <p>
     *     Preferred parametrized constructor to set the value of the
     *     menu options, possible choices and stream
     * </p>
     * @param options Options for the menu
     * @param choices Possible choices list
     * @param stream Stream for the menu
     */
    public MainMenu(String options, final List<String> choices, Stream stream) {
        this.options = options;
        this.choices = choices;
        this.stream = stream;
        this.parent = null;
    }

    /**
     * Gets the calling parent for the menu
     * @return parent {@link Menu}
     */
    @Override
    public Menu getParent() {
        return parent;
    }

    /**
     * Gets the choices for display
     * @return list of string choices
     */
    @Override
    public List<String> getChoices() {
        return choices;
    }

    /**
     * Gets the options for the menu
     * @return {@link String} options
     */
    @Override
    public String getOptions() {
        return options;
    }

    /**
     * Sets the parent menu for the given menu
     * @param parent specified parent {@link Menu}
     * @return {@link Menu}
     */
    @Override
    public MainMenu setParent(Menu parent) {
        this.parent = parent;
        return this;
    }

    /**
     * Gets the stream for the menu
     * @return {@link Stream}
     */
    @Override
    public Stream getStream() {
        return stream;
    }

    /**
     * <p>
     *     Runs the Main menu as in takes the user input and figures out
     *     which one is the selected option. Then, runs the selected OpsMenu. <br>
     *         Keeps on running until the user inputs a valid option.
     * </p>
     * @return {@link Menu} object
     * @throws DependencyException In case of unknown operational errors
     */
    @Override
    public Menu run() throws DependencyException {
        LOGGER.addContext("Method", "run");
        Menu selectedMenu;
        try {
            stream.output(toString());
            stream.output("Enter your option");
            String selected = stream.input();
            selectedMenu = select(SafeGet.integer(selected));
            selectedMenu.run();
            return this;
        } catch (DependencyException e) {
            stream.output("\n[ERROR] Possible choices are: " + choices + "\n");
            return run();
        } catch (Exception e) {
            LOGGER.error(e);
            throw new DependencyException(e);
        }
    }

    /**
     * Switches to the corresponding {@link MainMenu} item
     * @param option {@link Integer} option
     * @return {@link MainMenu}
     * @throws DependencyException In case an invalid option is selected
     */
    @Override
    public Menu select(Integer option)
            throws DependencyException {
        LOGGER.addContext("Method", "select");
        switch (option) {
            case 1:
                return CONTEXT.getBean("SurveyMenu", Menu.class)
                        .setParent(this);
            case 2:
                return CONTEXT.getBean("TestMenu", Menu.class)
                        .setParent(this);
            case 3:
                System.exit(0);
            default:
                LOGGER.error(new DependencyException());
                throw new DependencyException("Invalid option selected");
        }
    }

    /**
     * Override for the equals() method
     * @param o another object
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MainMenu)) return false;

        MainMenu mainMenu = (MainMenu) o;

        if (options != null ? !options.equals(mainMenu.options) : mainMenu.options != null) return false;
        if (choices != null ? !choices.equals(mainMenu.choices) : mainMenu.choices != null) return false;
        return stream != null ? stream.equals(mainMenu.stream) : mainMenu.stream == null;
    }

    /**
     * Override for the hashcode() method
     * @return int
     */
    @Override
    public int hashCode() {
        int result = options != null ? options.hashCode() : 0;
        result = 31 * result + (choices != null ? choices.hashCode() : 0);
        result = 31 * result + (stream != null ? stream.hashCode() : 0);
        return result;
    }

    /**
     * <p>
     *     Override for the toString() method to print the
     *     option list
     * </p>
     * @return String
     */
    @Override
    public String toString() {
        return "\n" + options + "\n";
    }

}
