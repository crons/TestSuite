package suite.model.menu;

import suite.model.Menu;
import suite.model.Option;
import suite.model.Question;
import suite.model.Suite;
import suite.model.options.RankingOptions;
import suite.model.question.QuestionTypes;
import suite.model.suite.SuiteTypes;
import suite.util.DependencyException;
import suite.util.LogContext;
import suite.util.SafeGet;
import suite.util.stream.Stream;

import java.util.List;

/**
 * <p>
 *     An abstraction to provide the options for Survey Questions
 *     and perform operations based on user-input
 * </p>
 *
 * @author Prakash
 */
public class SurveyMenu extends SuiteMenu implements Menu {

    private final LogContext LOGGER = new LogContext(SurveyMenu.class);
    protected final static String QUESTION_PROMPT
            = "Enter the prompt for the question";
    protected final static String NO_OF_CHOICES
            = "Enter the number of responses required";
    protected final static String NO_OF_OPTIONS
            = "Enter the number of options";

    /**
     * @deprecated Uses a deprecated base class constructor
     */
    @Deprecated
    public SurveyMenu() {
        super();
    }

    /**
     * <p>
     *     Parametrized constructor that gets the default values from the
     *     bean configuration
     * </p>
     * @param options Options for the menu
     * @param choices Choices for the menu
     * @param stream {@link Stream} for the menu
     * @param type {@link SuiteTypes}
     * @param suite {@link Suite}
     */
    public SurveyMenu(String options, List<String> choices, Stream stream, SuiteTypes type, Suite suite) {
        super(options, choices, stream, type);
        setSuite(suite);
    }

    /**
     * <p>
     *     Override for the select method() to execute the different questions
     *     based on the user input and saves them to the suite data member
     * </p>
     * @param option {@link Integer} option
     * @return {@link Menu} object
     * @throws DependencyException In case there are operational/edge-case errors
     */
    @Override
    public Menu select(Integer option)
            throws DependencyException {
        LOGGER.addContext("Method", "select");
        switch (option) {
            case 1:
                String prompt = getPrompt(QUESTION_PROMPT);
                Question question = CONTEXT.getBean("SurveyTrueFalse", Question.class)
                        .setPrompt(prompt);
                getSuite().addQuestion(question);
                ((SuiteMenu) getParent()).setSuite(getSuite());
                return this;

            case 2:
                prompt = getPrompt(QUESTION_PROMPT);
                question = CONTEXT.getBean("SurveyDefault", Question.class)
                        .setPrompt(prompt);
                getSuite().addQuestion(setOptions(question));
                ((SuiteMenu) getParent()).setSuite(getSuite());
                return this;

            case 3:
                prompt = getPrompt(QUESTION_PROMPT);
                question = CONTEXT.getBean("SurveyEssay", Question.class)
                        .setPrompt(prompt)
                        .setNumberOfChoices(getNumberOfOptions(NO_OF_CHOICES));
                if (question.getNumberOfChoices() > 1) {
                    question.setType(QuestionTypes.SURVEY_ESSAY_MULTIPLE);
                }
                getSuite().addQuestion(question);
                ((SuiteMenu) getParent()).setSuite(getSuite());
                return this;

            case 4:
                prompt = getPrompt(QUESTION_PROMPT);
                question = CONTEXT.getBean("SurveyShort", Question.class)
                        .setPrompt(prompt)
                        .setNumberOfChoices(getNumberOfOptions(NO_OF_CHOICES));
                if (question.getNumberOfChoices() > 1) {
                    question.setType(QuestionTypes.SURVEY_SHORT_MULTIPLE);
                }
                getSuite().addQuestion(question);
                ((SuiteMenu) getParent()).setSuite(getSuite());
                return this;

            case 5:
                prompt = getPrompt(QUESTION_PROMPT);
                question = CONTEXT.getBean("SurveyMatching", Question.class)
                        .setPrompt(prompt);
                getSuite().addQuestion(setMatchingOptions(question));
                ((SuiteMenu) getParent()).setSuite(getSuite());
                return this;

            case 6:
                prompt = getPrompt(QUESTION_PROMPT);
                question = CONTEXT.getBean("SurveyRanking", Question.class)
                        .setPrompt(prompt);
                getSuite().addQuestion(setRankingOptions(question));
                ((SuiteMenu) getParent()).setSuite(getSuite());
                return this;

            case 7:
                return getParent();

            default:
                LOGGER.error(new DependencyException());
                throw new DependencyException("Invalid option selected");
        }
    }

    /**
     * Gets the number of options
     * @param prompt Prompt
     * @return {@link Integer}
     */
    private Integer getNumberOfRankingOptions(String prompt) {
        getStream().output(prompt);
        try {
            return SafeGet.numberGreaterThan(2, getStream().input());
        } catch (DependencyException e) {
            getStream().output("\n[ERROR] Enter a number > 1 !\n");
            return getNumberOfRankingOptions(prompt);
        }
    }

    /**
     * Sets the {@link suite.model.Options} for the question
     * @param question specified {@link Question}
     * @return same {@link Question}
     */
    protected Question setOptions(Question question) {
        Integer options;
        Option[] optionList;
        try {
            options = getNumberOfOptions(NO_OF_OPTIONS);
            Integer choices = getNumberOfOptions(NO_OF_CHOICES);
            question.setNumberOfChoices(choices);
            if (choices > 1) {
                boolean survey = question.getType().isSurvey();
                question.setType((survey) ? QuestionTypes.SURVEY_MULTIPLE : QuestionTypes.TEST_MULTIPLE);
            }
            optionList = new Option[options];
            for (int i = 1; i <= options; i++) {
                try {
                    String option = getOption("Enter option #" + i);
                    Option opt = CONTEXT.getBean("Default", Option.class)
                            .setIdentifier("" + (char) (i + 64))
                            .setOption(option);
                    optionList[i-1] = opt;
                } catch (DependencyException e) {
                    i--;
                }
            }

            return question.setOptions(optionList);
        } catch (DependencyException e) {
            return setOptions(question);
        }
    }

    /**
     * Sets the matching type options to the specified {@link Question}
     * @param question specified {@link Question}
     * @return same {@link Question}
     */
    protected Question setMatchingOptions(Question question) {
        Integer options;
        Option[] optionList;
        try {
            options = getNumberOfRankingOptions("Enter the number of rows");
            question.setNumberOfChoices(options);
            optionList = new Option[options];
            for (int i = 1; i <= options; i++) {
                try {
                    Option option = CONTEXT.getBean("Matching", Option.class)
                            .setOption(getOption("Enter LHS for row #" + i))
                            .setIdentifier(getOption("Enter RHS for row #" + i))
                            .setOrder(i);
                    optionList[i-1] = option;
                } catch (DependencyException e) {
                    i--;
                }
            }

            return question.setOptions(optionList);
        } catch (DependencyException e) {
            return setMatchingOptions(question);
        }
    }

    /**
     * Sets the {@link RankingOptions} to the {@link Question}
     * @param question specified {@link Question}
     * @return same {@link Question}
     */
    protected Question setRankingOptions(Question question) {
        Integer options;
        Option[] optionList;
        try {
            options = getNumberOfRankingOptions(NO_OF_OPTIONS);
            question.setNumberOfChoices(options);
            optionList = new Option[options];
            for (int i = 1; i <= options; i++) {
                try {
                    Option option = CONTEXT.getBean("Ranking", Option.class)
                            .setOption(getOption("Enter the ranking option #" + i))
                            .setIdentifier(("" + (char)(64+i)))
                            .setOrder(i);
                    optionList[i-1] = option;
                } catch (DependencyException e) {
                    i--;
                }
            }

            return question.setOptions(optionList);
        } catch (DependencyException e) {
            return setRankingOptions(question);
        }
    }

}
