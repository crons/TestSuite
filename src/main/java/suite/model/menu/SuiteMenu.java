package suite.model.menu;

import suite.model.Answers;
import suite.model.Menu;
import suite.model.Option;
import suite.model.Question;
import suite.model.Suite;
import suite.model.option.RankingOption;
import suite.model.options.DefaultOptions;
import suite.model.question.QuestionTypes;
import suite.model.suite.SuiteTypes;
import suite.model.suite.Survey;
import suite.util.LogContext;
import suite.util.SafeGet;
import suite.util.Serializer;
import suite.util.stream.Stream;
import suite.util.DependencyException;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 *     Abstraction for the suite menu to provide the options
 *     related to the suites. Its kind of an operational menu.
 * </p>
 *
 * @author Prakash
 */
public class SuiteMenu extends MainMenu implements Menu {

    private final LogContext LOGGER = new LogContext(SuiteMenu.class);

    private final SuiteTypes suiteType;
    private Suite suite;

    /**
     * @deprecated Uses a deprecated base class constructor
     */
    @Deprecated
    public SuiteMenu() {
        super();
        suiteType = SuiteTypes.NONE;
    }

    /**
     * <p>
     *     Parametrized constructor to set the options from the bean
     *     configuration for the suite menu
     * </p>
     * @param options Options for the menu
     * @param choices List of possible choices
     * @param stream {@link Stream} for the menu
     * @param type {@link SuiteTypes}
     */
    public SuiteMenu(String options, List<String> choices, Stream stream, SuiteTypes type) {
        super(options, choices, stream);
        suiteType = type;
    }

    /**
     * Gets the suite for the Suite menu
     * @return {@link Suite} object
     */
    public Suite getSuite() {
        return suite;
    }

    /**
     * <p>
     *     Sets the suite for the suite menu. The same suite
     *     is carried out and from the {@link SurveyMenu} and
     *     {@link TestMenu} as well
     * </p>
     * @param suite {@link Suite} for the menu
     * @return {@link SuiteMenu}
     */
    public SuiteMenu setSuite(Suite suite) {
        this.suite = suite;
        return this;
    }

    /**
     * Gets the {@link SuiteTypes} that the menu handles
     * @return {@link SuiteTypes} enum
     */
    public SuiteTypes getSuiteType() {
        return suiteType;
    }

    /**
     * Override for the select option to lead to the correct beans
     * @param option {@link Integer} option
     * @return {@link Menu} object
     * @throws DependencyException In case there are operational/edge-case errors
     */
    @Override
    public Menu select(Integer option)
            throws DependencyException {
        LOGGER.addContext("Method", "select");
        switch (option) {
            case 1:
                String name = getName();
                Menu menu = null;

                switch (suiteType) {
                    case SURVEY:
                        menu = CONTEXT.getBean("SurveyQuestionMenu", Menu.class)
                                .setParent(this);
                        break;
                    case TEST:
                        menu = CONTEXT.getBean("TestQuestionMenu", Menu.class)
                                .setParent(this);
                }

                ((SuiteMenu) menu).getSuite().setName(name);
                return menu;

            case 2:
                getStream().display("\n\n" + suite + "\n");
                return this;

            case 3:
                Serializer.serialize(suite, suiteType);
                getStream().display("\nFile Saved!\n");
                return this;

            case 4:
                suite = Serializer.deserialize(getStream(), suiteType);
                getStream().display("\nFile Loaded!\n");
                return this;

            case 5:
                switch (suiteType) {
                    case SURVEY:
                        return ((SurveyMenu) CONTEXT.getBean("SurveyQuestionMenu", Menu.class))
                                .setSuite(suite)
                                .setParent(this);
                    case TEST:
                        return ((TestMenu) CONTEXT.getBean("TestQuestionMenu", Menu.class))
                                .setSuite(suite)
                                .setParent(this);
                }

            case 6:
                suite = modifySuite(
                        Serializer.deserialize(getStream(), suiteType)
                );
                Serializer.serialize(suite, suiteType);
                return this;

            case 7:
                suite = take(
                        Serializer.deserialize(getStream(), suiteType)
                );
                Serializer.serialize(suite, suiteType);
                return this;

            case 8:
                suite = Serializer.deserialize(getStream(), suiteType);
                getStream().display(suite.tabulate());
                return this;

            case 9:
                if (suiteType == SuiteTypes.SURVEY) {
                    return getParent();
                } else {
                    suite = Serializer.deserialize(getStream(), suiteType);
                    getStream().output(suite.grade());
                    return this;
                }

            case 10:
                if (suiteType == SuiteTypes.TEST) {
                    return getParent();
                }
                System.exit(0);

            case 11:
                if (suiteType == SuiteTypes.SURVEY) {
                    LOGGER.error(new DependencyException());
                    throw new DependencyException("Invalid option for survey");
                }
                System.exit(0);

            default:
                LOGGER.error(new DependencyException());
                throw new DependencyException("Invalid option");
        }
    }

    /**
     * Gets the name from the user by means of {@link SafeGet}
     * @return {@link String} name
     */
    protected String getName() {
        getStream().output("Enter the name for the suite");
        try {
            return SafeGet.string(getStream().input());
        } catch (DependencyException e) {
            getStream().output("[ERROR] Enter the name!\n");
            return getName();
        }
    }

    /**
     * Modifies the given {@link Survey}
     * @param suite {@link Suite} needs to be a {@link Survey}
     * @return same {@link Suite}
     */
    protected Suite modifySuite(Suite suite) {
        boolean flag = false;
        Question question = selectQuestion(suite);
        Answers answers = suite.getQuestions().get(question);
        suite.getQuestions().remove(question);
        if (choice("Do you wish to modify the question? [y/n]")) {
            getStream().output("\n" + question.getPrompt() + "\n");
            question.setPrompt(getPrompt("Enter the new prompt for the question"));
            flag = true;
        }
        while (!question.getType().isNonModifiableOptions() && choice("Do you wish to modify the options? [y/n]")) {
            if (question.getType().isRanking() || question.getType().isMatching()) {
                modifyRankingOption(question);
            } else {
                modifyOption(question);
            }
            flag = true;
        }
        if (suite.getType().isTest()) {
            while (!question.getType().isNonModifiableAnswers() && choice("Do you wish to modify the answers? [y/n]")) {
                flag = true;
                if (question.getType().isShortAnswer()) {
                    getCorrectShortAnswers(question, true);
                } else if (question.getType().isMatching() || question.getType().isRanking()) {
                    setCorrectRankings(question, true);
                } else {
                    setCorrectOptions(question, true);
                }
            }
        }
        suite.getQuestions().put(question, answers);
        return flag ? suite.clearTabulation(question) : suite;
    }

    /**
     * Selects the {@link Question} based on the user input
     * @param suite {@link Suite}
     * @return same {@link Suite}
     */
    protected Question selectQuestion(Suite suite) {
        try {
            getStream().output("Enter the question");
            return SafeGet.question(SafeGet.string(getStream().input()), suite);
        } catch (DependencyException e) {
            getStream().output("\n[ERROR] Possible choices are:\n" + suite.getPossibleQuestions() + "\n");
            return selectQuestion(suite);
        }
    }

    /**
     * Modifies the option of the given {@link Question}
     * @param question specified {@link Question}
     * @return {@link Option}
     */
    protected Option modifyOption(Question question) {
        try {
            getStream().output("\n" + question.getOptions());
            getStream().output("\nWhich option do you wish to modify?");
            return selectOption(question, 0, 1, new LinkedList<>())
                    .setOption(
                            getOption("Enter the new option text")
                    );
        } catch (DependencyException de) {
            getStream().output("\n[ERROR] Possible choices are: " + question.getPossibleChoices() + "\n");
            return modifyOption(question);
        }
    }

    /**
     * Modifies the ranking/matching options
     * @param question specified {@link Question}
     * @return {@link Option}
     */
    protected Option modifyRankingOption(Question question) {
        try {
            QuestionTypes type = question.getType();
            String prompt = type.isRanking() ? "Enter the option no. you want to modify" :
                    "Enter the row no. you want to modify";
            question.getOptions().options().forEach( option -> {
                if (type.isRanking()) {
                    getStream().output("\t" + option.getOrder() + ". " + option.getOption());
                } else {
                    getStream().output("\t" + option.getOrder() + ". " + option.getOption() + "\t\t" + option.getIdentifier());
                }
            });
            Option option = SafeGet.getRankingOption(
                    getNumberOfOptions("\n" + prompt),
                    question
            );
            return modifyRankingOption(option, type.isRanking());
        } catch (DependencyException e) {
            getStream().output("\n[ ERROR ] Possible choices are : [ 1 - " + question.getNumberOfChoices() + " ]\n");
            return modifyRankingOption(question);
        }
    }

    /**
     * Modifies the ranking/matching option
     * @param option specified {@link Option}
     * @param ranking Whether it is ranking/matching
     * @return {@link Option}
     */
    private Option modifyRankingOption(Option option, boolean ranking) {
        if (ranking) {
            option.setOption(
                    getOption("Enter the new ranking option text")
            );
        } else {
            option.setOption(
                    getOption("Enter the new LHS")
            ).setIdentifier(
                    getOption("Enter the new RHS")
            );
        }
        return option;
    }

    /**
     * Gets the prompt from the user and passes it through Safeget
     * @param prompt Prompt to the user
     * @return {@link String} prompt
     */
    protected String getPrompt(String prompt) {
        getStream().output(prompt);
        try {
            return SafeGet.string(getStream().input());
        } catch (DependencyException e) {
            getStream().output("[ERROR] Enter the prompt!\n");
            return getPrompt(prompt);
        }
    }

    /**
     * @param prompt Prompt to the user
     * @return Whether the user choice is true or false
     */
    protected boolean choice(String prompt) {
        getStream().output(prompt);
        try {
            return SafeGet.choice(getStream().input());
        } catch (DependencyException e) {
            getStream().output("[ERROR] Possible choices are: [y, Y, n, N]\n");
            return choice(prompt);
        }
    }

    /**
     * Gets the text for the option after displaying the specified prompt
     * @param prompt Prompt
     * @return {@link String}
     */
    protected String getOption(String prompt) {
        getStream().output(prompt);
        try {
            return SafeGet.string(getStream().input());
        } catch (DependencyException e) {
            getStream().output("[ERROR] Enter the option text!\n");
            return getOption(prompt);
        }
    }

    /**
     * Method to help the user take the {@link Suite}
     * @param suite specified {@link Suite}
     * @return same {@link Suite}
     * @throws DependencyException In case of operational errors
     */
    private Suite take(Suite suite) throws DependencyException {
        LOGGER.withContext("Method", "take")
                .addContext("Suite", suite.toString());
        try {
            for (Question question : suite.getQuestionsList()) {
                getStream().output("\n" + question + "\n");
                if (question.getType().isMatching() || question.getType().isRanking()) {
                    selectRank(question, suite);
                } else {
                    Option[] options = new Option[question.getNumberOfChoices()];
                    for (int i = 0; i < question.getNumberOfChoices(); i++) {
                        Option option = (question.getType().isEssay() || question.getType().isShortAnswer()) ?
                                selectEssayOption(
                                        question,
                                        i + 1,
                                        question.getNumberOfChoices(),
                                        Arrays.stream(options)
                                                .parallel()
                                                .filter(Objects::nonNull)
                                                .collect(Collectors.toList())
                                ) :
                                selectOption(
                                        question,
                                        i + 1,
                                        question.getNumberOfChoices(),
                                        Arrays.stream(options)
                                                .parallel()
                                                .filter(Objects::nonNull)
                                                .collect(Collectors.toList())
                                );
                        options[i] = option;
                    }
                    suite.addAnswer(
                            question,
                            options
                    );
                }
            }
            return suite;
        } catch (DependencyException e) {
            LOGGER.fatal(e);
            throw new DependencyException("Fatal error while taking Suite", e);
        }
    }

    /**
     * Helps the user select the {@link Option} utilizing {@link SafeGet}
     * @param question specified {@link Question}
     * @param number index of the option
     * @param total total number of choices
     * @param options list of {@link Option} to ignore
     * @return {@link Option}
     */
    protected Option selectOption(Question question, int number, int total, List<Option> options) {
        String prompt = (total == 1) ? "Enter the option identifier" :
                "Enter the option identifier #" + number ;
        getStream().output(prompt);
        try {
            String identifier = SafeGet.string(getStream().input());
            Option option = SafeGet.option(identifier, question);
            if (options.contains(option)) {
                throw new DependencyException("Option already chosen!");
            }
            return option;
        } catch (DependencyException e) {
            getStream().output("\n[ERROR] Possible choices are: " + question.getPossibleSelections(options) + "\n");
            return selectOption(question, number, total, options);
        }
    }

    /**
     * Helps the user select the {@link Option} for essay/short-answer utilizing {@link SafeGet}
     * @param question specified {@link Question}
     * @param number index of the option
     * @param total total number of choices
     * @param options list of {@link Option} to ignore
     * @return {@link Option}
     */
    protected Option selectEssayOption(Question question, int number, int total, List<Option> options) {
        String prompt = (total == 1) ? "Enter the response" :
                "Enter the response #" + number ;
        getStream().output(prompt);
        try {
            String text = SafeGet.string(getStream().input());
            Option option = (question.getType().isEssay() ?
                    CONTEXT.getBean("Essay", Option.class) :
                    CONTEXT.getBean("Short", Option.class))
                    .setOption(text);
            if (options.contains(option)) {
                throw new DependencyException("Option already chosen!");
            }
            return option;
        } catch (DependencyException e) {
            getStream().output("\n[ERROR] Enter a unique response ! \n");
            return selectEssayOption(question, number, total, options);
        }
    }

    /**
     * Gets the correct Rank by employing {@link SafeGet}
     * @param prompt Prompt
     * @param question specified {@link Question}
     * @return String
     */
    protected String getCorrectRank(String prompt, Question question) {
        getStream().output(prompt);
        try {
            return SafeGet.rank(getStream().input(), question);
        } catch (DependencyException e) {
            getStream().output("\n[ERROR] Possible choices are: " + question.getPossibleChoices() + "\n");
            return getCorrectRank(prompt, question);
        }
    }

    /**
     * Gets the correct Rank response by employing {@link SafeGet}
     * @param prompt Prompt
     * @param question specified {@link Question}
     * @param options to ignore
     * @return String
     */
    protected String getCorrectRankResponse(String prompt, Question question, List<Option> options) {
        getStream().output(prompt);
        try {
            return SafeGet.rankResponse(getStream().input(), question, options);
        } catch (DependencyException e) {
            getStream().output("\n[ERROR] Possible choices are: " + question.getPossibleSelections(options) + "\n");
            return getCorrectRankResponse(prompt, question, options);
        }
    }

    /**
     * Sets the answer to the matching/ranking question
     * @param question {@link Question}
     * @param suite {@link Suite}
     * @return same {@link Suite}
     */
    private Suite selectRank(Question question, Suite suite) {
        try {
            List<Option> options = question.getOptions().options();
            List<Option> optionsToIgnore = new LinkedList<>();
            RankingOption[] rankingOptions = new RankingOption[question.getNumberOfChoices()];
            for (int i = 0; i < question.getNumberOfChoices(); i++) {
                try {
                    Option option = options.get(i);
                    RankingOption rankingOption = (RankingOption) (question.getType().isRanking() ?
                            CONTEXT.getBean("Ranking", Option.class) :
                            CONTEXT.getBean("Matching", Option.class))
                            .setOption(option.getOption())
                            .setIdentifier(option.getIdentifier())
                            .setOrder(option.getOrder());
                    String prompt = (question.getType().isRanking()) ?
                            "Enter rank for " + option :
                            "Enter match for \"" + option.getOption() + "\"";
                    if (question.getType().isTest()) {
                        rankingOptions[i] = rankingOption.setRank(
                                getCorrectRankResponse(prompt, question, optionsToIgnore)
                        );
                        optionsToIgnore.add(rankingOptions[i]);
                    } else {
                        rankingOptions[i] = rankingOption.setRank(
                                getCorrectRank(prompt, question)
                        );
                    }
                } catch (DependencyException e) {
                    i--;
                }
            }
            return suite.addAnswer(question, rankingOptions);
        } catch (DependencyException e) {
            return selectRank(question, suite);
        }
    }

    /**
     * Sets the correct option to the question
     * @param question {@link Question} to set the option for
     * @param number Number of correct option (0 if does not apply)
     * @param options Total number of options
     * @param correctOptions Correct {@link Option} list
     * @return {@link Question} again
     */
    protected Question setCorrectOption(Question question, int number, int options, List<Option> correctOptions) {
        String prompt = (options == 1) ? "Enter the correct option identifier" :
                "Enter correct option identifier #" + number ;
        getStream().output(prompt);
        String identifier = getStream().input();
        try {
            question = SafeGet.questionWithCorrectOption(question, identifier);
            correctOptions.addAll(question.getOptions().correct().options());
            return question;
        } catch (DependencyException e) {
            getStream().output("\n[ERROR] Possible choices are: " + question.getPossibleSelections(correctOptions) + "\n");
            return setCorrectOption(question, number, options, correctOptions);
        }
    }

    /**
     * <p>
     *     Set multiple correct options for default questions
     * </p>
     * @param question {@link Question}
     * @param modifying Modifying or setting
     * @return same {@link Question}
     */
    protected Question setCorrectOptions(Question question, boolean modifying) {
        try {
            if (modifying) {
                question.getOptions().clean();
            }
            List<Option> correctOptions = new LinkedList<>();
            for (int i = 1; i <= question.getNumberOfChoices(); i++) {
                setCorrectOption(question, i, question.getNumberOfChoices(), correctOptions);
            }
            return question;
        } catch (DependencyException e) {
            return setCorrectOptions(question, modifying);
        }
    }

    /**
     * Gets the correct short answer from the admin of the test
     * @param question {@link Question}
     * @param modifying Modifying or setting
     * @return same {@link Question}
     */
    protected Question getCorrectShortAnswers(Question question, boolean modifying) {
        Integer options;
        Option[] optionList;
        try {
            if (modifying) {
                question.getOptions().clean();
            }
            if (!modifying) {
                options = getNumberOfOptions("Enter the number of correct answers");
                if (options > 1) {
                    question.setType(QuestionTypes.TEST_SHORT_MULTIPLE);
                }
                question.setNumberOfChoices(options);
            }
            optionList = new Option[question.getNumberOfChoices()];
            for (Integer i = 1; i <= question.getNumberOfChoices(); i++) {
                String answer = getOption((question.getNumberOfChoices() == 1) ? "Enter the correct answer" : "Enter the correct answer #" + i);
                Option option = CONTEXT.getBean("Short", Option.class)
                        .setOption(answer);
                optionList[i-1] = option;
            }
            return SafeGet.questionWithCorrectOption(question, optionList);
        } catch (DependencyException e) {
            return getCorrectShortAnswers(question, modifying);
        }
    }

    /**
     * Sets the correct {@link RankingOption} for the {@link Question}
     * @param question specified {@link Question}
     * @param modifying Modifying or setting
     * @return same {@link Question}
     */
    protected Question setCorrectRankings(Question question, boolean modifying) {
        try {
            if (modifying) {
                question.getOptions().clean();
            }
            List<Option> options = question.getOptions().options();
            for (int i = 0; i < options.size(); i++) {
                try {
                    Option option = options.get(i);
                    String prompt = (question.getType().isRanking()) ?
                            "Enter correct rank for " + option :
                            "Enter correct match for \"" + option.getOption() + "\"";

                    question.setCorrectOption(
                            option.setRank(
                                    getCorrectRank(prompt, question)
                    ));
                } catch (DependencyException e) {
                    i--;
                }
            }
            return question;
        } catch (DependencyException e) {
            return setCorrectRankings(question, modifying);
        }
    }

    /**
     * Gets the number of options
     * @param prompt Prompt
     * @return {@link Integer}
     */
    protected Integer getNumberOfOptions(String prompt) {
        getStream().output(prompt);
        try {
            return SafeGet.numberGreaterThan(1, getStream().input());
        } catch (DependencyException e) {
            getStream().output("\n[ERROR] Enter a number > 0 !\n");
            return getNumberOfOptions(prompt);
        }
    }

}
