package suite.model.menu;

import suite.model.Menu;
import suite.model.Option;
import suite.model.Question;
import suite.model.Suite;
import suite.model.option.RankingOption;
import suite.model.question.QuestionTypes;
import suite.model.suite.SuiteTypes;
import suite.util.DependencyException;
import suite.util.LogContext;
import suite.util.SafeGet;
import suite.util.stream.Stream;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * <p>
 *     Abstraction for the Test Menu to give the options
 *     related to test questions
 * </p>
 *
 * @author Prakash
 */
public class TestMenu extends SurveyMenu implements Menu {

    private final LogContext LOGGER = new LogContext(TestMenu.class);

    /**
     * @deprecated uses a deprecated base class constructor
     */
    @Deprecated
    public TestMenu() {
        super();
    }

    /**
     * <p>
     *     Parametrized constructor to set the default values from
     *     the bean configuration
     * </p>
     * @param options Options for the Test menu
     * @param choices Possible choices
     * @param stream {@link Stream} for the menu
     * @param type {@link SuiteTypes}
     * @param suite {@link Suite} for the menu
     */
    public TestMenu(String options, List<String> choices, Stream stream, SuiteTypes type, Suite suite) {
        super(options, choices, stream, type, suite);
    }

    /**
     * <p>
     *     Select() method override for the option provided
     *     by the user
     * </p>
     * @param option {@link Integer} option
     * @return {@link Menu}
     * @throws DependencyException In case of operational errors
     */
    @Override
    public Menu select(Integer option) throws DependencyException {
        LOGGER.addContext("Method", "select");
        switch (option) {
            case 1:
                String prompt = getPrompt(QUESTION_PROMPT);
                Question question = CONTEXT.getBean("TestTrueFalse", Question.class)
                        .setPrompt(prompt);
                getSuite().addQuestion(setCorrectOption(question, 0, 1, new LinkedList<>()));
                ((SuiteMenu) getParent()).setSuite(getSuite());
                return this;

            case 2:
                prompt = getPrompt(QUESTION_PROMPT);
                question = CONTEXT.getBean("TestDefault", Question.class)
                        .setPrompt(prompt);
                getSuite().addQuestion(
                        setCorrectOptions(
                                setOptions(question),
                                false
                        )
                );
                ((SuiteMenu) getParent()).setSuite(getSuite());
                return this;

            case 3:
                prompt = getPrompt(QUESTION_PROMPT);
                question = CONTEXT.getBean("TestEssay", Question.class)
                        .setPrompt(prompt)
                        .setNumberOfChoices(getNumberOfOptions(NO_OF_CHOICES));
                if (question.getNumberOfChoices() > 1) {
                    question.setType(QuestionTypes.TEST_ESSAY_MULTIPLE);
                }
                getSuite().addQuestion(question);
                ((SuiteMenu) getParent()).setSuite(getSuite());
                return this;

            case 4:
                prompt = getPrompt(QUESTION_PROMPT);
                question = CONTEXT.getBean("TestShort", Question.class)
                        .setPrompt(prompt)
                        .setNumberOfChoices(getNumberOfOptions(NO_OF_CHOICES));
                if (question.getNumberOfChoices() > 1) {
                    question.setType(QuestionTypes.TEST_SHORT_MULTIPLE);
                }
                getSuite().addQuestion(getCorrectShortAnswers(question, false));
                ((SuiteMenu) getParent()).setSuite(getSuite());
                return this;

            case 5:
                prompt = getPrompt(QUESTION_PROMPT);
                question = CONTEXT.getBean("TestMatching", Question.class)
                        .setPrompt(prompt);
                getSuite().addQuestion(
                        setCorrectRankings(
                                setMatchingOptions(question),
                                false
                        ));
                ((SuiteMenu) getParent()).setSuite(getSuite());
                return this;

            case 6:
                prompt = getPrompt(QUESTION_PROMPT);
                question = CONTEXT.getBean("TestRanking", Question.class)
                        .setPrompt(prompt);
                getSuite().addQuestion(
                        setCorrectRankings(
                                setRankingOptions(question),
                                false
                        ));
                ((SuiteMenu) getParent()).setSuite(getSuite());
                return this;

            case 7:
                return getParent();

            default:
                LOGGER.error(new DependencyException());
                throw new DependencyException("Invalid option selected");
        }
    }

}
