package suite.model;

import suite.util.DependencyException;
import suite.util.stream.Stream;

import java.util.List;

/**
 * <p>
 *     Used to stream the menu and accept the user input
 *     using the same stream. Provides the interface for
 *     the main program
 * </p>
 *
 * @author Prakash
 */

public interface Menu {

    /**
     * <p>
     *     Runs the menu to display the prompt taking into account the
     *     {@link suite.util.stream.Stream} and also switches between
     *     the menus if needed;
     * </p>
     * @return a {@link Menu} link
     * @throws DependencyException In case of operational errors while handling edge cases
     */
    Menu run()
            throws DependencyException;

    /**
     * Switches between the menus or the tasks based on the user input
     * @param option {@link Integer} option
     * @return corresponding {@link Menu}
     * @throws DependencyException In case there are operational errors
     */
    Menu select(Integer option)
            throws DependencyException;

    /**
     * Gets the stream for the menu
     * @return {@link Stream}
     */
    Stream getStream();

    /**
     * Gets the parent menu
     * @return {@link Menu} object
     */
    Menu getParent();

    /**
     * Sets the parent menu of the child
     * @param parent parent {@link Menu}
     * @return {@link Menu}
     */
    Menu setParent(Menu parent);

    /**
     * Gets the choices for display
     * @return list of string choices
     */
    List<String> getChoices();

    /**
     * Sets the options for
     * @return
     */
    String getOptions();

}
