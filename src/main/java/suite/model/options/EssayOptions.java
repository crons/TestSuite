package suite.model.options;

import suite.model.Option;
import suite.model.Options;
import suite.model.option.OptionTypes;
import suite.model.question.QuestionTypes;
import suite.util.DependencyException;
import suite.util.LogContext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 *     Abstraction to store details about the short answer/essay type
 *     responses for a question
 * </p>
 *
 * @author Prakash
 */
public class EssayOptions implements Options, java.io.Serializable {

    private QuestionTypes type;
    private Map<Option, OptionTypes> options;

    private final LogContext LOGGER = new LogContext(EssayOptions.class);

    /**
     * Default constructor
     */
    public EssayOptions() {
        type = QuestionTypes.NONE;
        options = new HashMap<>();
    }

    /**
     * Parametrized constructor to set the type parameters with the specified
     * values
     * @param type {@link QuestionTypes} for the Options
     * @param options {@link Option} list specified
     * @throws DependencyException In case of operational errors
     */
    public EssayOptions(QuestionTypes type, List<Option> options)
            throws DependencyException {
        LOGGER.addContext("Method", "init");
        this.type = type;
        this.options = new HashMap<>();
        try {
            for (Option option : options) {
                this.options.put(
                        option,
                        (type.isMultiple()) ?
                                ((type.isSurvey()) ? OptionTypes.SURVEY : ((type.isEssay()) ? OptionTypes.ESSAYMULTIPLE : OptionTypes.SHORTINCORRECT)) :
                                ((type.isSurvey()) ? OptionTypes.SURVEY : ((type.isEssay()) ? OptionTypes.ESSAY : OptionTypes.SHORTINCORRECT))
                );
            }
        } catch (Exception e) {
            LOGGER.error(e);
            throw new DependencyException(e);
        }
    }

    /**
     * Gets the {@link QuestionTypes} for the Options
     * @return {@link QuestionTypes}
     */
    @Override
    public QuestionTypes getType() {
        return type;
    }

    /**
     * Sets the Question type for the Options
     * @param type {@link QuestionTypes}
     * @return {@link EssayOptions}
     */
    @Override
    public EssayOptions setType(QuestionTypes type) {
        this.type = type;
        return this;
    }

    /**
     * Gets the options mapping contained by the {@link EssayOptions}
     * @return Mapping of {@link Option} -> {@link OptionTypes}
     */
    public Map<Option, OptionTypes> getOptions() {
        return options;
    }

    /**
     * Sets the options mapping
     * @param options Mapping of {@link Option} -> {@link OptionTypes}
     * @return {@link EssayOptions}
     */
    public EssayOptions setOptions(Map<Option, OptionTypes> options) {
        this.options = options;
        return this;
    }

    /**
     * <p>
     *     Sets the mapping for the option parameters (if they are
     *     already in the mapping) to <b>CORRECT</b>
     * </p>
     * @param options {@link Option} stream
     * @return {@link EssayOptions}
     * @throws DependencyException In case the specified options are ungradable
     */
    @Override
    public EssayOptions set(Option... options) throws DependencyException {
        LOGGER.addContext("Method", "set");
        for (Option option: options) {
            if (type.isGraded() && type.isTest() && type.isShortAnswer() && contains(option)) {
                option.setType((type.isMultiple()) ? OptionTypes.SHORTMULTICORRECT : OptionTypes.SHORTCORRECT);
                this.options.put(
                        option,
                        (type.isMultiple()) ? OptionTypes.SHORTMULTICORRECT : OptionTypes.SHORTCORRECT);
            } else {
                LOGGER.error(new DependencyException());
                throw new DependencyException("Implemeting setCorrectOption() on ungradable object");
            }
        }
        return this;
    }

    /**
     *
     * @return
     * @throws DependencyException
     */
    @Override
    public EssayOptions clean() throws DependencyException {
        try {
            options().forEach(option -> {
                OptionTypes types = (type.isMultiple()) ?
                        ((type.isSurvey()) ? OptionTypes.SURVEY : ((type.isEssay()) ? OptionTypes.ESSAYMULTIPLE : OptionTypes.SHORTINCORRECT)) :
                        ((type.isSurvey()) ? OptionTypes.SURVEY : ((type.isEssay()) ? OptionTypes.ESSAY : OptionTypes.SHORTINCORRECT));
                option.setType(types);
                this.options.put(
                        option,
                        types
                );
            });
            return this;
        } catch (Exception e) {
            throw new DependencyException();
        }
    }

    /**
     * Checks to see whether the specified {@link Option} is present
     * @param options {@link Option} objects
     * @return boolean
     */
    @Override
    public boolean contains(Option... options) {
        for (Option option: options) {
            if (!this.options.containsKey(option)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Not applicable in this case
     * @return {@link EssayOptions}
     * @throws DependencyException Unsupported operations
     */
    @Override
    public EssayOptions refresh() throws DependencyException {
        LOGGER.withContext("Method", "refresh")
                .fatal(new DependencyException());
        throw new DependencyException();
    }

    /**
     * @return count of the options
     */
    @Override
    public int size() {
        return options.size();
    }

    /**
     * <p>
     *     Verifies whether the given option is the correct option  by
     *     looking at the list of options in this object
     * </p>
     * @param options specified {@link Option}
     * @return boolean
     * @throws DependencyException In case the specified options are not gradable
     */
    @Override
    public boolean verify(Option... options) throws DependencyException {
        LOGGER.addContext("Method", "verify");
        Integer correct = 0;
        for (Option option: options) {
            if (type.isGraded() && type.isTest() && type.isShortAnswer()) {
                if (correct().options().parallelStream().filter(option1 -> option.equals(option1)).count() > 0) {
                    correct++;
                }
            } else {
                LOGGER.error(new DependencyException());
                throw new DependencyException("Ungradable object in isCorrect method");
            }
        }
        return correct == options.length;
    }

    /**
     * Gets the correct {@link EssayOptions}
     * @return {@link EssayOptions}
     * @throws DependencyException In case of operational errors
     */
    @Override
    public EssayOptions correct() throws DependencyException {
        LOGGER.addContext("Method", "correct");
        try {
            return new EssayOptions(
                    type,
                    options().stream()
                            .filter(option -> option.getType().isCorrect())
                            .collect(Collectors.toList())
            );
        } catch (Exception e) {
            LOGGER.error(e);
            throw new DependencyException("Error while getting the correct options", e);
        }
    }

    /**
     * Gets the singleton list of the binding {@link Option}
     * @return {@link Option} list
     */
    @Override
    public List<Option> options() {
        return options
                .keySet()
                .stream()
                .collect(Collectors.toList());
    }

    /**
     * Override for the equals()
     * @param o another {@link Object}
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EssayOptions)) return false;

        EssayOptions that = (EssayOptions) o;

        if (options() != null ? !options().equals(that.options()) : that.options() != null) return false;
        return type == that.type;
    }

    /**
     * Override for the hashcode()
     * @return hash
     */
    @Override
    public int hashCode() {
        int result = options() != null ? options().hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    /**
     * Override for the toString() to just include the option text
     * @return {@link String}
     */
    @Override
    public String toString() {
        String string = "";
        for (Option option: options()) {
            string += option + "\n";
        }
        return string;
    }

}
