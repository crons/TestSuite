package suite.model.options;

import suite.model.Option;
import suite.model.Options;
import suite.model.option.OptionTypes;
import suite.model.option.RankingOption;
import suite.model.question.QuestionTypes;
import suite.util.DependencyException;
import suite.util.LogContext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 *     Abstraction over ranking options for a question. <br>
 *         Generally, stores the option in the mapping format
 *         <code>{ option -> optionRank }</code>
 * </p>
 *
 * @author Prakash
 */
public class RankingOptions implements Options, java.io.Serializable {

    private Map<Option, String> options;
    private QuestionTypes type;

    private final LogContext LOGGER = new LogContext(RankingOptions.class);

    /**
     * <p>
     *     Default constructor that sets the value of options
     *     to an <b>empty map</b> and question type is set to
     *     <b>NONE</b>
     * </p>
     */
    public RankingOptions() {
        options = new HashMap<>();
        type = QuestionTypes.NONE;
    }

    /**
     * <p>
     *     Parametrized constructor to set the options and
     *     question type
     * </p>
     * @param options {@link Option} stream
     * @param type {@link QuestionTypes} value
     * @exception DependencyException In case of error with mapping
     */
    public RankingOptions(QuestionTypes type, List<Option> options)
            throws DependencyException {
        LOGGER.addContext("Method", "<init>");
        this.type = type;
        this.options = new HashMap<>();
        try {
            for (Option option: options) {
                this.options.put(option, "N/A");
            }
        } catch (Exception e) {
            LOGGER.error(e);
            throw new DependencyException(e);
        }
    }

    /**
     * @return Mapping of {@link Option} -> Rank {@link String}
     */
    public Map<Option, String> getOptions() {
        return options;
    }

    /**
     * Sets the option to ranking mapping
     * @param options {@link Option} -> {@link String} mapping
     * @return {@link RankingOptions}
     */
    public RankingOptions setOptions(Map<Option, String> options) {
        this.options = options;
        return this;
    }

    /**
     * @return {@link QuestionTypes}
     */
    @Override
    public QuestionTypes getType() {
        return type;
    }

    /**
     * Sets the question type for the option
     * @param type {@link QuestionTypes}
     * @return {@link RankingOptions}
     */
    @Override
    public RankingOptions setType(QuestionTypes type) {
        this.type = type;
        return this;
    }

    /**
     * <p>
     *     Sets the mapping for the option parameters (if they are
     *     already in the mapping) to <b>RANK</b>
     * </p>
     * @param options {@link Option} stream
     * @return {@link RankingOptions}
     * @throws DependencyException If the option is not compatible to grading
     */
    @Override
    public RankingOptions set(Option... options)
            throws DependencyException {
        LOGGER.addContext("Method", "set");
        for (Option option: options) {
            if (type.isGraded() && type.isTest() && contains(option)) {
                option.setType((type.isRanking()) ? OptionTypes.RANKCORRECT : OptionTypes.MATCHCORRECT);
                RankingOption op = (RankingOption) option;
                this.options.put(option, op.getRank());
            } else {
                LOGGER.error(new DependencyException());
                throw new DependencyException("Can't set the ranking option");
            }
        }
        return this;
    }

    /**
     * Refreshes the options to the correct configuration
     * @return {@link RankingOptions}
     */
    @Override
    public RankingOptions refresh() {
        options().forEach(
                option -> {
                    String rank = option.getRank();
                    options.replace(option, rank);
                }
        );
        return this;
    }

    /**
     * Gets the correct {@link RankingOptions}
     * @return {@link RankingOptions}
     * @throws DependencyException In case of streaming errors
     */
    @Override
    public RankingOptions correct()
            throws DependencyException {
        LOGGER.addContext("Method", "correct");
        try {
            return new RankingOptions(
                    type,
                    options().stream()
                            .filter(option -> option.getType().isCorrect())
                            .collect(Collectors.toList())
            );
        } catch (Exception e) {
            LOGGER.error(e);
            throw new DependencyException("Error while getting the correct options", e);
        }
    }

    /**
     * Gets the {@link Option} list from the options
     * @return {@link Option} list
     */
    @Override
    public List<Option> options() {
        return options
                .keySet()
                .stream()
                .collect(Collectors.toList());
    }

    /**
     * <p>
     *     Checks whether the specified options are correct or not. <br>
     *         The options provided need to be gradable
     * </p>
     * @param options specified {@link Option}
     * @return boolean
     * @throws DependencyException In case the object is not gradable
     */
    @Override
    public boolean verify(Option... options)
            throws DependencyException {
        LOGGER.addContext("Method", "verify");
        for (Option option: options) {
            if ((type.isRanking() || type.isMatching()) && option instanceof RankingOption) {
                Option option1 = this.options.keySet()
                        .parallelStream()
                        .filter(option2 -> option2.getOrder() == option.getOrder())
                        .findFirst()
                        .get();
                if (!this.options.get(option1).equals(option.getRank())) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * <p>
     *     Checks to see whether the {@link Options} instance contains
     *     the specified {@link Option} objects
     * </p>
     * @param options {@link Option} objects
     * @return boolean
     */
    @Override
    public boolean contains(Option... options) {
        for (Option option: options) {
            if (!this.options.containsKey(option)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Gets the count of the options
     * @return size (int)
     */
    @Override
    public int size() {
        return options.size();
    }

    /**
     * Cleans the correct options
     * @return {@link RankingOptions}
     * @throws DependencyException Operational errors
     */
    @Override
    public RankingOptions clean() throws DependencyException {
        try {
            options().forEach(option -> {
                option.setRank("N/A");
                options.put(option, "N/A");
            });
            return this;
        } catch (Exception e) {
            throw new DependencyException();
        }
    }



    /**
     * Override for the basic equals() method
     * @param o Any {@link Object}
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RankingOptions)) return false;

        RankingOptions that = (RankingOptions) o;

        if (options() != null ? !options().equals(that.options()) : that.options() != null) return false;
        return type == that.type;
    }

    /**
     * Override for the hashcode
     * @return hashcode
     */
    @Override
    public int hashCode() {
        int result = options() != null ? options().hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    /**
     * <p>
     *     toString() override to display the options in the following format: <br>
     *         A) Option 1 B) Option 2 ...
     * </p>
     * @return String
     * @exception DependencyException In case the user misses an option
     */
    @Override
    public String toString()
            throws DependencyException {
        LOGGER.withContext("Method", "toString")
                .addContext("Options", options.toString());
        String string = "";
        try {
            List<Option> options = options();
            for(Option option: options) {
                string += option + "\n";
            }
            return string;
        } catch (NullPointerException e) {
            LOGGER.error(e);
            throw new DependencyException("Missing an option", e);
        }
    }

}
