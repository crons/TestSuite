package suite.model.options;

import suite.model.Option;
import suite.model.Options;
import suite.model.option.OptionTypes;
import suite.model.question.QuestionTypes;
import suite.util.DependencyException;
import suite.util.LogContext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 *     Abstraction over Option interface to contain multiple
 *     options and keep them as a mapping with <br>
 *         { option -> optionType }
 * </p>
 * @author Prakash
 */
public class DefaultOptions implements Options, java.io.Serializable {

    private Map<Option, OptionTypes> options;
    private QuestionTypes type;

    private final LogContext LOGGER = new LogContext(DefaultOptions.class);

    /**
     * <p>
     *     Default constructor that sets the value of options
     *     to an <b>empty map</b> and question type is set to
     *     <b>NONE</b>
     * </p>
     */
    public DefaultOptions() {
        options = new HashMap<>();
        type = QuestionTypes.NONE;
    }

    /**
     * <p>
     *     Parametrized constructor to set the options and
     *     question type
     * </p>
     * @param options {@link Option} stream
     * @param type {@link QuestionTypes} value
     * @exception DependencyException In case of error with mapping
     */
    public DefaultOptions(QuestionTypes type, List<Option> options)
            throws DependencyException {
        LOGGER.addContext("Method", "<init>");
        this.type = type;
        this.options = new HashMap<>();
        try {
            for (Option option : options) {
                this.options.put(
                        option,
                        (type.isSurvey()) ? OptionTypes.SURVEY : OptionTypes.INCORRECT);
            }
        } catch (Exception e) {
            LOGGER.error(e);
            throw new DependencyException(e);
        }
    }

    /**
     * <p>
     *     Gets the mapping of the {@link Option} and
     *     {@link OptionTypes}
     * </p>
     * @return Mapping of {@link Option} -> {@link OptionTypes}
     */
    public Map<Option, OptionTypes> getOptions() {
        return options;
    }

    /**
     * Sets the options and their option type mappings
     * @param options Mapping {@link Option} -> {@link OptionTypes}
     * @returnn {@link DefaultOptions}
     */
    public DefaultOptions setOptions(Map<Option, OptionTypes> options) {
        this.options = options;
        return this;
    }

    /**
     * Gets the type of the question
     * @return {@link QuestionTypes}
     */
    @Override
    public QuestionTypes getType() {
        return type;
    }

    /**
     * Sets the question type for the options
     * @param type {@link QuestionTypes}
     * @return {@link DefaultOptions}
     */
    @Override
    public DefaultOptions setType(QuestionTypes type) {
        this.type = type;
        return this;
    }

    /**
     * <p>
     *     Sets the mapping for the option parameters (if they are
     *     already in the mapping) to <b>CORRECT</b>
     * </p>
     * @param options {@link Option} stream
     * @return {@link DefaultOptions}
     * @throws DependencyException If the option is not compatible to grading
     */
    @Override
    public DefaultOptions set(Option... options)
            throws DependencyException {
        LOGGER.addContext("Method", "set");
        for (Option option: options) {
            if (type.isGraded() && type.isTest() && !type.isRanking() && contains(option)) {
                option.setType((type.isMultiple()) ? OptionTypes.MULTICORRECT : OptionTypes.CORRECT);
                this.options.put(
                        option,
                        (type.isMultiple()) ? OptionTypes.MULTICORRECT : OptionTypes.CORRECT);
            } else {
                LOGGER.error(new DependencyException());
                throw new DependencyException("Implemeting setCorrectOption() on ungradable object");
            }
        }
        return this;
    }

    /**
     * Cleans the correct options
     * @return {@link DefaultOptions}
     * @throws DependencyException Operational errors
     */
    @Override
    public DefaultOptions clean() throws DependencyException {
        try {
            options().forEach(option -> {
                option.setType(type.isSurvey() ? OptionTypes.SURVEY : OptionTypes.INCORRECT);
                options.put(
                        option,
                        (type.isSurvey()) ? OptionTypes.SURVEY : OptionTypes.INCORRECT);
            });
            return this;
        } catch (Exception e) {
            throw new DependencyException();
        }
    }

    /**
     * Gets the {@link Option} list from the options
     * @return {@link Option} list
     */
    @Override
    public List<Option> options() {
        return options
                .keySet()
                .stream()
                .collect(Collectors.toList());
    }

    /**
     * Gets the correct {@link DefaultOptions}
     * @return {@link DefaultOptions}
     * @throws DependencyException In case of streaming errors
     */
    @Override
    public DefaultOptions correct()
            throws DependencyException {
        LOGGER.addContext("Method", "correct");
        try {
            List<Option> options = options().stream()
                    .filter(option -> option.getType().isCorrect())
                    .collect(Collectors.toList());
            return new DefaultOptions(
                    type,
                    options
            );
        } catch (Exception e) {
            LOGGER.error(e);
            throw new DependencyException("Error while getting the correct options", e);
        }
    }

    /**
     * <p>
     *     Checks whether the specified options are correct or not. <br>
     *         The options provided need to be gradable
     * </p>
     * @param options specified {@link Option}
     * @return boolean
     * @throws DependencyException In case the object is not gradable
     */
    @Override
    public boolean verify(Option... options)
            throws DependencyException {
        LOGGER.addContext("Method", "verify");
        for (Option option: options) {
            if (type.isGraded() && type.isTest() && !type.isRanking() && contains(option)) {
                if (!this.options.get(option).isCorrect()) {
                    return false;
                }
            } else {
                LOGGER.error(new DependencyException());
                throw new DependencyException("Ungradable object in isCorrect method");
            }
        }
        return true;
    }

    /**
     * Refreshes the current option to correct configuration;
     * not applicable in this case
     * @return {@link Options}
     * @throws DependencyException Unsupported Operation
     */
    @Override
    public DefaultOptions refresh() throws DependencyException {
        LOGGER.withContext("Method", "refresh")
                .fatal(new DependencyException());
        throw new DependencyException();
    }

    /**
     * <p>
     *     Checks to see whether the {@link Options} instance contains
     *     the specified {@link Option} objects
     * </p>
     * @param options {@link Option} objects
     * @return boolean
     */
    @Override
    public boolean contains(Option... options) {
        for (Option option: options) {
            if (!this.options.containsKey(option)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Gets the count of the options
     * @return size (int)
     */
    @Override
    public int size() {
        return options.size();
    }

    /**
     * Override for the basic equals() method
     * @param o Any {@link Object}
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DefaultOptions)) return false;

        DefaultOptions that = (DefaultOptions) o;

        if (options() != null ? !options().equals(that.options()) : that.options() != null) return false;
        return type == that.type;
    }

    /**
     * Override for the hashcode
     * @return hashcode
     */
    @Override
    public int hashCode() {
        int result = options() != null ? options().hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    /**
     * <p>
     *     toString() override to display the options in the following format: <br>
     *         A) Option 1 B) Option 2 ...
     * </p>
     * @return String
     * @exception DependencyException In case the user misses an option, especially in case of essays
     */
    @Override
    public String toString()
            throws DependencyException {
        LOGGER.withContext("Method", "toString")
                .addContext("Options", options.toString());
        String string = "";
        try {
            List<Option> options = options();
            for(Option option: options) {
                string += option;
            }
            return string;
        } catch (NullPointerException e) {
            LOGGER.error(e);
            throw new DependencyException("Missing an option", e);
        }
    }

}
