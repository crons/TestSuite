package suite.model.answers;

import suite.model.Answers;
import suite.model.Option;
import suite.model.Options;
import suite.model.options.EssayOptions;
import suite.model.question.QuestionTypes;
import suite.util.DependencyException;
import suite.util.LogContext;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *     Abstraction to store the tabulations and users response
 *     to an essay/short-answer type question
 * </p>
 * @author Prakash
 */
public class EssayAnswers extends DefaultAnswers implements Answers, java.io.Serializable {

    private final LogContext LOGGER = new LogContext(EssayAnswers.class);

    /**
     * Default constructor
     * @deprecated Use parametrized constructor instead
     */
    @Deprecated
    public EssayAnswers() {
        super();
    }

    /**
     * Parametrized constructor with {@link QuestionTypes}
     * @param type {@link QuestionTypes}
     */
    public EssayAnswers(QuestionTypes type) {
        tabulation = new HashMap<>();
        this.type = type;
        choice = new EssayOptions().setType(type);
    }

    /**
     * Parametrized constructor to set the correct options tabulations and type
     * @param type {@link QuestionTypes}
     * @param options {@link Option} list
     * @throws DependencyException In case of operational errors in {@link DefaultAnswers}
     */
    public EssayAnswers(QuestionTypes type, List<Option> options)
            throws DependencyException {
        super(type, options);
    }

    /**
     * Sets the current choice for the user and also presets it in tabulation
     * @param choice {@link Options}
     * @return {@link EssayAnswers}
     * @throws DependencyException In case options are not found in tabultaion
     */
    @Override
    public EssayAnswers setChoice(Options choice) throws DependencyException {
        LOGGER.addContext("Method", "setChoice");
        try {
            this.choice = choice;
            choice.options().forEach(option -> {
                if (tabulation.containsKey(option)) {
                    Integer faceValue = tabulation.get(option) + 1;
                    tabulation.put(option, faceValue);
                } else {
                    tabulation.put(option, 1);
                }
            });
            return this;
        } catch (Exception e) {
            LOGGER.fatal(e);
            throw new DependencyException("Option does not exist", e);
        }
    }

    /**
     * Sets the current choice for the user and also presets it in tabulation
     * @param choice stream of {@link Option}
     * @return {@link EssayAnswers}
     * @throws DependencyException In case an error is returned by setChoice(Options)
     */
    @Override
    public EssayAnswers setChoice(Option... choice) throws DependencyException {
        LOGGER.withContext("Method", "setChoice")
                .addContext("Options", Arrays.deepToString(choice));
        EssayOptions options = new EssayOptions(type, Arrays.asList(choice));
        return setChoice(options);
    }

    /**
     * Override for toString()
     * @return {@link String}
     */
    @Override
    public String toString() {
        if (tabulation.size() == 0) {
            return "\tNo responses recorded yet !\n\n";
        }
        StringBuilder str = new StringBuilder(type.isEssay() ? "" : "\n[ Option ] -> [ Frequency ]\n\n");
        tabulation.forEach((option, integer) -> {
            if (!type.isEssay()) {
                str.append("\t" + option + "\t->\t" + integer + "\n");
            } else {
                str.append("\t" + option + "\n\n");
            }
        });
        return str.toString();
    }

}
