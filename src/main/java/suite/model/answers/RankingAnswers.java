package suite.model.answers;

import suite.model.Answers;
import suite.model.Option;
import suite.model.Options;
import suite.model.option.RankingOption;
import suite.model.options.RankingOptions;
import suite.model.question.QuestionTypes;
import suite.util.DependencyException;
import suite.util.LogContext;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *     Abstraction to contains users answers and tabulations
 *     for ranking/matching questions
 * </p>
 * @author Prakash
 */
public class RankingAnswers extends DefaultAnswers implements Answers, java.io.Serializable {

    private final LogContext LOGGER = new LogContext(RankingAnswers.class);

    private Map<RankingOptions, Integer> ranks;

    /**
     * Default constructor
     * @throws DependencyException unsupported operation
     * @deprecated unsupported operation
     */
    @Deprecated
    public RankingAnswers() throws DependencyException {
        LOGGER.withContext("Method", "<init>")
                .fatal(new DependencyException());
        throw new DependencyException("Unsupported Operation");
    }

    /**
     * Parametrized constructor
     * @param type {@link QuestionTypes}
     * @param options {@link Option} list
     * @throws DependencyException unsupported operation
     * @deprecated unsupported operation
     */
    @Deprecated
    public RankingAnswers(QuestionTypes type, List<Option> options)
            throws DependencyException {
        LOGGER.withContext("Method", "<init>")
                .fatal(new DependencyException());
        throw new DependencyException("Unsupported Operation");
    }

    /**
     * Parametrized constructor used for surveys
     * @param type {@link QuestionTypes}
     */
    public RankingAnswers(QuestionTypes type) {
        tabulation = Collections.EMPTY_MAP;
        choice = new RankingOptions();
        this.type = type;
        ranks = new HashMap<>();
    }

    /**
     * Parametrized constructor to set the {@link QuestionTypes} and
     * {@link RankingOptions}
     * @param type {@link QuestionTypes}
     * @param options {@link RankingOptions}
     * @throws DependencyException In case of operational errors
     */
    public RankingAnswers(QuestionTypes type, RankingOptions options)
            throws DependencyException {
        LOGGER.addContext("Method", "<init>");
        try {
            this.type = type;
            tabulation = Collections.EMPTY_MAP;
            choice = new RankingOptions();
            ranks = new HashMap<>();
            ranks.put(options, 0);
        } catch (Exception e) {
            LOGGER.fatal(e);
            throw new DependencyException(e);
        }
    }

    /**
     * Gets the tabulation but unsupported in this case
     * @return Map of {@link Option} -> {@link Integer}
     * @throws DependencyException unsupported operation
     * @deprecated Unsupported operation
     */
    @Deprecated
    @Override
    public Map<Option, Integer> getTabulation() throws DependencyException {
        LOGGER.withContext("Method", "getTabulation")
                .fatal(new DependencyException());
        throw new DependencyException("Unsupported Operation");
    }

    /**
     * Sets the tabulation for the question
     * @param tabulation Mapping of {@link Option} -> {@link Integer}
     * @return {@link RankingAnswers}
     * @throws DependencyException unsupported operation
     * @deprecated Unsupported operation
     */
    @Deprecated
    @Override
    public RankingAnswers setTabulation(Map<Option, Integer> tabulation)
            throws DependencyException {
        LOGGER.withContext("Method", "setTabulation")
                .fatal(new DependencyException());
        throw new DependencyException("Unsupported Operation");
    }

    /**
     * Sets the response for the user
     * @param choice {@link Options}
     * @return {@link RankingAnswers}
     * @throws DependencyException In case of operational errors
     */
    @Override
    public RankingAnswers setChoice(Options choice) throws DependencyException {
        LOGGER.addContext("Method", "setChoice");
        try {
            this.choice = choice;
            Integer faceValue = 1;
            if (ranks.containsKey(choice)) {
                faceValue = ranks.get(choice) + 1;
            }
            ranks.put((RankingOptions)choice, faceValue);
            return this;
        } catch (Exception e) {
            LOGGER.fatal(e);
            throw new DependencyException("Error while setting the choice", e);
        }
    }

    /**
     * Sets the choice for the user based on the input stream
     * @param choice stream of {@link Option}
     * @return {@link RankingAnswers}
     * @throws DependencyException In case of operational errors
     */
    @Override
    public RankingAnswers setChoice(Option... choice) throws DependencyException {
        RankingOptions options = new RankingOptions(type, Arrays.asList(choice));
        return setChoice(options);
    }

    /**
     * Gets the ranks for the question
     * @return Map of {@link RankingOptions} -> {@link Integer}
     */
    @Override
    public Map<RankingOptions, Integer> getRanks() {
        return ranks;
    }

    /**
     * Sets the ranks for the question
     * @param ranks Map of {@link RankingOptions} -> {@link Integer}
     * @return {@link RankingAnswers}
     */
    @Override
    public RankingAnswers setRanks(Map<RankingOptions, Integer> ranks) {
        this.ranks = ranks;
        return this;
    }

    /**
     * To tabulate the ranking options
     * @param rank Rank needed
     * @param integer from rannks map
     * @param options key from ranks map
     * @return {@link String}
     */
    private String displayRanking(int rank, int integer, RankingOptions options) {
        StringBuilder str = new StringBuilder("");
        options.options().forEach(
                option -> {
                    if (("" + rank).equals(option.getRank())) {
                        if (rank == 1) {
                            str.append("\t[ " + option.getOption() + " ]\t->\t" + integer + "\n");
                        } else {
                            str.append("\t[ " + option.getOption() + " ]\n");
                        }
                    }
                }
        );
        return str.toString();
    }

    /**
     * Override for toString()
     * @return {@link String}
     */
    @Override
    public String toString() {
        if (ranks.size() == 0) {
            return "No responses have been recorded yet !";
        }
        StringBuilder str = new StringBuilder("\n[ Option ] -> [ Frequency ]\n\n");
        ranks.forEach((options, integer) -> {
            for (int i = 0; i < options.size(); i++) {
                RankingOption option = (RankingOption) options.options().get(i);
                if (type.isRanking()) {
                    str.append(displayRanking(i + 1, integer, options));
                    continue;
                }
                if (i == 0) {
                    str.append("\t[ " + option.getOption() + " - " + option.getRank() + " ]\t->\t" + integer + "\n");
                } else {
                    str.append("\t[ " + option.getOption() + " - " + option.getRank() + " ]\n");
                }
            }
            str.append("\n");
        });
        return str.toString();
    }
}
