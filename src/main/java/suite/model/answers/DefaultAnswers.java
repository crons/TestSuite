package suite.model.answers;

import suite.model.Answers;
import suite.model.Option;
import suite.model.Options;
import suite.model.options.DefaultOptions;
import suite.model.options.RankingOptions;
import suite.model.question.QuestionTypes;
import suite.util.DependencyException;
import suite.util.LogContext;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *     Abstraction to contain a mapping of {@link Option} -> {@link Integer}
 *     to include the frequency. Also, contains the current responses by the
 *     user.
 * </p>
 *
 * @author Prakash
 */
public class DefaultAnswers implements Answers, java.io.Serializable {

    private final LogContext LOGGER = new LogContext(DefaultAnswers.class);

    protected Map<Option, Integer> tabulation;
    protected Options choice;
    protected QuestionTypes type;

    /**
     * Default constructor
     */
    public DefaultAnswers() {
        tabulation = new HashMap<>();
        choice = new DefaultOptions();
        type = QuestionTypes.NONE;
    }

    /**
     * Parametrized constructor to set the list of tabulations
     * @param type {@link QuestionTypes}
     * @param options list of {@link Option}
     * @throws DependencyException In case of operational errors
     */
    public DefaultAnswers(QuestionTypes type, List<Option> options)
            throws DependencyException {
        LOGGER.addContext("Method", "<init>");
        try {
            this.type = type;
            choice = new DefaultOptions();
            tabulation = new HashMap<>();
            options.forEach(option -> tabulation.put(option, 0));
        } catch (Exception e) {
            LOGGER.error(e);
            throw new DependencyException("Operation error while construction");
        }
    }

    /**
     * Gets the tabulation of the responses to teh question
     * @return Mapping of {@link Option} -> {@link Integer}
     */
    @Override
    public Map<Option, Integer> getTabulation() {
        return tabulation;
    }

    /**
     * Sets the mapping of {@link Option} -> {@link Integer}
     * @param tabulation Mapping of {@link Option} -> {@link Integer}
     * @return {@link DefaultAnswers}
     */
    @Override
    public DefaultAnswers setTabulation(Map<Option, Integer> tabulation) {
        this.tabulation = tabulation;
        return this;
    }

    /**
     * Gets the users correct choice
     * @return {@link Options}
     */
    @Override
    public Options getChoice() {
        return choice;
    }

    /**
     * Sets the current choice for the user
     * @param choice {@link Options}
     * @return {@link DefaultOptions}
     * @throws DependencyException In case the options are not found in the tabulation
     */
    @Override
    public DefaultAnswers setChoice(Options choice) throws DependencyException {
        LOGGER.addContext("Method", "setChoice");
        try {
            this.choice = choice;
            choice.options().forEach(option -> {
                Integer faceValue = tabulation.get(option) + 1;
                tabulation.put(option, faceValue);
            });
            return this;
        } catch (Exception e) {
            LOGGER.fatal(e);
            throw new DependencyException("Option does not exist", e);
        }
    }

    /**
     * Sets the current choice for the user
     * @param choice stream of {@link Option}
     * @return {@link DefaultAnswers}
     * @throws DependencyException In case an error is returned by setChoice(Options)
     */
    @Override
    public DefaultAnswers setChoice(Option... choice) throws DependencyException {
        LOGGER.withContext("Method", "setChoice")
            .addContext("Options", Arrays.deepToString(choice));
        DefaultOptions options = new DefaultOptions(type, Arrays.asList(choice));
        return setChoice(options);
    }

    /**
     * @return Map of {@link RankingOptions} -> {@link Integer}
     * @throws DependencyException because it does not apply
     */
    @Override
    public Map<RankingOptions, Integer> getRanks() throws DependencyException {
        LOGGER.addContext("Method", "getRanks");
        LOGGER.fatal(new DependencyException());
        throw new DependencyException("getRanks() can't be used with DefaultAnswers");
    }

    /**
     * @param ranks Map of {@link RankingOptions} -> {@link Integer}
     * @return {@link DefaultAnswers}
     * @throws DependencyException because it does not apply
     */
    @Override
    public DefaultAnswers setRanks(Map<RankingOptions, Integer> ranks) throws DependencyException {
        LOGGER.addContext("Method", "setRanks");
        LOGGER.fatal(new DependencyException());
        throw new DependencyException("setRanks() can't be used with DefaultAnswers");
    }

    /**
     * Gets the {@link QuestionTypes} for the anser
     * @return {@link QuestionTypes}
     */
    @Override
    public QuestionTypes getType() {
        return type;
    }

    /**
     * Sets the {@link QuestionTypes} for the answer
     * @param type {@link QuestionTypes}
     * @return {@link DefaultAnswers}
     */
    @Override
    public DefaultAnswers setType(QuestionTypes type) {
        this.type = type;
        return this;
    }

    /**
     * Override for the equals() method
     * @param o another {@link Object}
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DefaultAnswers that = (DefaultAnswers) o;

        if (tabulation != null ? !tabulation.equals(that.tabulation) : that.tabulation != null) return false;
        if (choice != null ? !choice.equals(that.choice) : that.choice != null) return false;
        return type == that.type;
    }

    /**
     * Override for the hashcode
     * @return hashcode
     */
    @Override
    public int hashCode() {
        int result = tabulation != null ? tabulation.hashCode() : 0;
        result = 31 * result + (choice != null ? choice.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    /**
     * Override for the toString() to display tabulation
     * @return {@link String}
     */
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder("\n[ Option ] -> [ Frequency ]\n\n");
        tabulation.forEach((option, integer) -> {
            str.append("\t" + option + "\t->\t" + integer + "\n");
        });
        return str.toString();
    }

}
