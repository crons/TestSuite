package suite.model.option;

import suite.model.Option;
import suite.util.LogContext;

/**
 * <p>
 *     Represents the ranking option that usually contains
 *     an option text and a rank
 * </p>
 *
 * @author Prakash
 */
public class RankingOption extends DefaultOption implements Option, java.io.Serializable {

    private final LogContext LOGGER = new LogContext(RankingOption.class);

    private String rank;
    private Integer order;

    /**
     * Default Constructor
     */
    public RankingOption() {
        super();
    }

    /**
     * Parametrized constructor for bean configuration
     * @param type {@link OptionTypes}
     */
    public RankingOption(OptionTypes type) {
        super(type);
    }

    /**
     * Parametrized constructor to set the option text and rank
     * @param option Option text
     * @param identifier Identifier
     */
    public RankingOption(String option, String identifier) {
        super(option, identifier);
        this.rank = "-1";
        setType(OptionTypes.RANKINCORRECT);
    }

    /**
     * Gets the rank
     * @return Integer
     */
    @Override
    public String getRank() {
        return rank;
    }

    /**
     * Sets the rank
     * @param rank Rank
     * @return {@link RankingOption}
     */
    @Override
    public RankingOption setRank(String rank) {
        this.rank = rank;
        return this;
    }

    /**
     * Gets the order for the option
     * @return {@link Integer} order
     */
    @Override
    public Integer getOrder() {
        return order;
    }

    /**
     * Sets the order for the option
     * @param order {@link Integer} order
     * @return {@link RankingOption}
     */
    @Override
    public RankingOption setOrder(Integer order) {
        this.order = order;
        return this;
    }

    /**
     * Override for the basic equals() method
     * @param o Object
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        RankingOption that = (RankingOption) o;

        if (rank != null ? !rank.equals(that.rank) : that.rank != null) return false;
        return order != null ? order.equals(that.order) : that.order == null;
    }

    /**
     * Override for hashcode()
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return order != null ? order.hashCode() : 0;
    }

    /**
     * Override for the toString () to handle different outputs for
     * matching and ranking styles
     * @return {@link String}
     */
    @Override
    public String toString() {
        if (getType() == OptionTypes.MATCHCORRECT || getType() == OptionTypes.MATHCINCORRECT) {
            return "\t\t" + getOption() + "\t\t | \t\t " + getIdentifier();
        } else {
            return "\t\t" + getIdentifier() + ") " + getOption();
        }
    }

}
