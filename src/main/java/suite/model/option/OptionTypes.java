package suite.model.option;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * <p>
 *     Enumeration class to represent the different types
 *     of options, specifically which of them are correct.
 * </p>
 *
 * @author Prakash
 */

public enum OptionTypes implements java.io.Serializable {

    SURVEY(false),
    CORRECT(true),
    MULTICORRECT(true),
    INCORRECT(false),
    RANKINCORRECT(false),
    RANKCORRECT(true),
    ESSAY(true),
    ESSAYMULTIPLE(true),
    SHORTINCORRECT(false),
    SHORTCORRECT(true),
    SHORTMULTICORRECT(true),
    MATHCINCORRECT(false),
    MATCHCORRECT(true)
    ;

    private final boolean correct;

    /**
     * Parametrized constructor
     * @param correct boolean
     */
    OptionTypes(final boolean correct) {
        this.correct = correct;
    }

    /**
     * Gets whether the option is correct
     * @return boolean
     */
    public boolean isCorrect() {
        return correct;
    }

    /**
     * Override for toString()
     * @return {@link String}
     */
    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(
                this,
                ToStringStyle.MULTI_LINE_STYLE
        );
    }

}
