package suite.model.option;

import suite.model.Option;
import suite.model.options.DefaultOptions;
import suite.util.DependencyException;
import suite.util.LogContext;

/**
 * <p>
 *     Default options generally contain the multiple choice
 *     questions text. <br>
 *         Used by {@link DefaultOptions} mostly.
 * </p>
 *
 * @author Prakash
 */
public class DefaultOption implements Option, java.io.Serializable {

    private final LogContext LOGGER = new LogContext(DefaultOption.class);

    private String option;
    private String identifier;
    private OptionTypes type;

    /**
     * Default constructor
     */
    public DefaultOption() {
        this.type = OptionTypes.INCORRECT;
    }

    /**
     * Parametrized constructor for bean configurations
     * @param type {@link OptionTypes}
     */
    public DefaultOption(OptionTypes type) {
        this.type = type;
    }

    /**
     * Parametrized constructor to set the option text
     * @param option Option text
     */
    public DefaultOption(String option, String identifier) {
        this.option = option;
        this.identifier = identifier;
        this.type = OptionTypes.INCORRECT;
    }

    /**
     * Gets the option text
     * @return Option text
     */
    @Override
    public String getOption() {
        return option;
    }

    /**
     * Sets the option text
     * @param option String option
     * @return {@link DefaultOption}
     */
    @Override
    public DefaultOption setOption(String option) {
        this.option = option;
        return this;
    }

    /**
     * Gets the identifier
     * @return String identifier
     */
    @Override
    public String getIdentifier() {
        return identifier;
    }

    /**
     * Sets the order; unsupported in this case
     * @param order {@link Integer} order
     * @return {@link DefaultOption}
     * @throws DependencyException Unsupported operation
     */
    @Override
    public DefaultOption setOrder(Integer order) throws DependencyException {
        LOGGER.withContext("Method", "setOrder")
                .fatal(new DependencyException());
        throw new DependencyException();
    }

    /**
     * Gets the Order for the option
     * @return {@link Integer} order
     * @throws DependencyException Unsupported operation
     */
    @Override
    public Integer getOrder() throws DependencyException {
        LOGGER.withContext("Method", "getOrder")
                .fatal(new DependencyException());
        throw new DependencyException();
    }

    /**
     * Sets the string identifier
     * @param identifier String identifier
     * @return {@link DefaultOption}
     */
    @Override
    public DefaultOption setIdentifier(String identifier) {
        this.identifier = identifier;
        return this;
    }

    /**
     * Gets the option type
     * @return {@link OptionTypes}
     */
    @Override
    public OptionTypes getType() {
        return type;
    }

    /**
     * Sets the option type
     * @param type {@link OptionTypes}
     * @return {@link Option}
     */
    @Override
    public DefaultOption setType(OptionTypes type) {
        this.type = type;
        return this;
    }

    /**
     * Gets the rank but unsupported in this case
     * @return {@link String}
     * @throws DependencyException Unsupported operation
     */
    @Override
    public String getRank() throws DependencyException {
        LOGGER.withContext("Method", "getRank")
                .fatal(new DependencyException());
        throw new DependencyException();
    }

    /**
     * Sets the rank but unsupported in this case
     * @param rank {@link String}
     * @return {@link DefaultOption}
     * @throws DependencyException Unsupported operation
     */
    @Override
    public DefaultOption setRank(String rank) throws DependencyException {
        LOGGER.withContext("Method", "setRank")
                .fatal(new DependencyException());
        throw new DependencyException();
    }

    /**
     * Override for the basic equals() method
     * @param o Object
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DefaultOption)) return false;

        DefaultOption that = (DefaultOption) o;

        if (getOption() != null ? !getOption().equalsIgnoreCase(that.getOption()) : that.getOption() != null) return false;
        return getIdentifier() != null ? getIdentifier().equalsIgnoreCase(that.getIdentifier()) : that.getIdentifier() == null;
    }

    /**
     * Override for the hashcode
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return (getIdentifier() != null ? getIdentifier().hashCode() : 0);
    }

    /**
     * Override of toString() to return the option text
     * @return Option text
     */
    @Override
    public String toString() {
        if (identifier.equalsIgnoreCase(option)) {
            return "[ " + option + " ]";
        }
        return identifier + ") " + option + "\t";
    }

}
