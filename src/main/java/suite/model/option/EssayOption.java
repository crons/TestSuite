package suite.model.option;

import suite.model.Option;

/**
 * <p>
 *     Abstraction to represent the essay option to store
 *     the essay response.
 * </p>
 *
 * @author Prakash
 */
public class EssayOption extends DefaultOption implements Option, java.io.Serializable {

    /**
     * Default Constructor
     */
    public EssayOption() {
        super();
    }

    /**
     * Parametrized constructor to set the value from bean
     * @param type {@link OptionTypes}
     */
    public EssayOption(OptionTypes type) {
        super(type);
    }

    /**
     * Parametrized constructor to set the default values
     * @param option Text for the option
     * @param identifier Identifier for the option (N/A) in this case
     */
    public EssayOption(String option, String identifier) {
        super(option, identifier);
    }

    /**
     * Override for the equals() method
     * @param o Object
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EssayOption)) return false;

        EssayOption that = (EssayOption) o;

        return getOption() != null ? getOption().equalsIgnoreCase(that.getOption()) : that.getOption() == null;
    }

    /**
     * Override for the toString() to not include the identifier
     * @return {@link String}
     */
    @Override
    public String toString() {
        return "\"" + getOption() + "\"";
    }

}
