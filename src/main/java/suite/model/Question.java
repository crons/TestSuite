package suite.model;

import suite.model.question.QuestionTypes;
import suite.util.DependencyException;

import java.util.List;

/**
 * <p>
 *     Abstraction for the questions handled by the test
 *     suite. <br>
 *         <ul>
 *             <li>Questions contain a {@link QuestionTypes} to distinguish
 *             and to establish a grading pattern</li>
 *             <li>Questions take a stream for input and output</li>
 *             <li>The question type for a question is fixed always</li>
 *             <li>The options can also be provided</li>
 *         </ul>
 * </p>
 *
 * @author Prakash
 */

public interface Question {

    /**
     * Sets the prompt for the question
     * @param prompt {@link String} for the question
     * @return {@link Question}
     */
    Question setPrompt(String prompt);

    /**
     * Gets the prompt for the question
     * @return {@link String}
     */
    String getPrompt();

    /**
     * <p>
     *     Sets the options for the question taking a
     *     {@link Options} object
     * </p>
     * @param options {@link Options} object
     * @return {@link Question}
     */
    Question setOptions(Options options);

    /**
     * Returns the correct options
     * @return {@link String}
     * @exception DependencyException In case of Ranking question
     */
    String answer() throws DependencyException;

    /**
     * <p>
     *     Sets the options for the question with a stream
     *     of {@link Option} objects
     * </p>
     * @param options {@link Option} stream
     * @return {@link Question}
     */
    Question setOptions(Option... options);

    /**
     * Gets the options for a question
     * @return {@link Options}
     */
    Options getOptions();

    /**
     * <p>
     *     Gets the option from the list with the given identifier.
     *     Must have an option with the same identifier
     * </p>
     * @param identifier String identifier
     * @return {@link Option} object
     * @exception DependencyException In case the option is not found
     */
    Option getOption(String identifier) throws DependencyException;

    /**
     * <p>
     *     Gets a list of identifiers for the possible choices. Especially useful
     *     when the user inputs a wrong choice
     * </p>
     * @return List of Identifiers
     */
    List<String> getPossibleChoices();

    /**
     * <p>
     *     Sets the correct option to the {@link Question} based
     *     on the specified {@link Option} object
     * </p>
     * @param option specified {@link Option}
     * @return {@link Question}
     * @throws DependencyException In case the question can't be graded
     */
    Question setCorrectOption(Option... option)
            throws DependencyException;

    /**
     * <p>
     *     Sets the correct option to the {@link Question} based
     *     on the {@link String} identifier provided
     * </p>
     * @param identifier String identifier
     * @return {@link Question}
     * @throws DependencyException In case the identifier is invalid choice
     */
    Question setCorrectOption(String... identifier)
            throws DependencyException;

    /**
     * Gets the type of the question
     * @return {@link QuestionTypes}
     */
    QuestionTypes getType();

    /**
     * Sets the question type for the question
     * @param type specified {@link QuestionTypes}
     * @return {@link Question}
     */
    Question setType(QuestionTypes type);

    /**
     * Sets the number of correct options
     * @param numberOfChoices Number of correct options
     * @return {@link Question}
     */
    Question setNumberOfChoices(Integer numberOfChoices);

    /**
     * Gets the number of correct options
     * @return {@link Integer}
     */
    Integer getNumberOfChoices();

    /**
     * <p>
     *     Gets a selection of identifiers after removing the specified list
     *     for streaming purposes
     * </p>
     * @param optionsToIgnore List of {@link Option} to ignore
     * @return List of identifiers
     */
    List<String> getPossibleSelections(List<Option> optionsToIgnore);

}
