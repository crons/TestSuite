package suite.model;

import suite.model.suite.SuiteTypes;
import suite.util.DependencyException;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *     Abstraction for the suite class used by the menus to
 *     keep a mapping of questions and answers. {@link suite.model.suite.Survey}
 *     and {@link suite.model.suite.Test} are the two child classes
 * </p>
 *
 * @author Prakash
 */

public interface Suite {

    /**
     * Gets the questions in the suite model as a list
     * @return List of {@link Question}
     */
    List<Question> getQuestionsList();

    /**
     * Gets the question list as a mapping
     * @return Map of {@link Question} -> {@link Options}
     */
    Map<Question, Answers> getQuestions();

    /**
     * Add the question(s) to the given suite model
     * @param questions Stream of {@link Question}
     * @return {@link Suite}
     * @exception DependencyException In case of operational errors in {@link Answers}
     */
    Suite addQuestion(Question... questions) throws DependencyException;

    /**
     * Gets the name for the suite
     * @return Name
     */
    String getName();

    /**
     * Sets the name for the suite
     * @param name Name
     * @return {@link Suite}
     */
    Suite setName(String name);

    /**
     * Add correct Option(s) for the given question
     * @param question {@link Question}
     * @param options correct stream of {@link Option}
     * @return {@link Suite}
     * @throws DependencyException In case it was not able to find the question
     */
    Suite addAnswer(Question question, Option... options)
            throws DependencyException;

    /**
     * Sets the questions with their correct options for the suite
     * @param questions Mapping {@link Question} -> {@link Answers}
     * @return {@link Suite}
     */
    Suite setQuestions(Map<Question, Answers> questions);

    /**
     * Gets the type for the suite
     * @return {@link SuiteTypes}
     */
    SuiteTypes getType();

    /**
     * Sets the type for the suite
     * @param type {@link SuiteTypes}
     * @return {@link Suite}
     */
    Suite setType(SuiteTypes type);

    /**
     * Gets the possible questions as a list of string
     * @return list of {@link String}
     */
    List<String> getPossibleQuestions();

    /**
     * Tabulate the {@link Suite}
     * @return {@link String}
     */
    String tabulate();

    /**
     * Returns the grade calculated for the {@link Suite}
     * @return Grade as a {@link String}
     * @throws DependencyException In case the suite is a {@link suite.model.suite.Survey} or operational errors
     */
    String grade() throws DependencyException;

    /**
     * Clears teh tabulation when the question is modified
     * @param question specified {@link Question}
     * @return {@link Suite}
     */
    Suite clearTabulation(Question question);

}
