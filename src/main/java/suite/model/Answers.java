package suite.model;

import suite.model.options.RankingOptions;
import suite.model.question.QuestionTypes;
import suite.util.DependencyException;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *     Interface for the answers used by the {@link Question} to
 *     include the tabulation as well as the current answer
 * </p>
 *
 * @author Prakash
 */
public interface Answers {

    /**
     * Gets the tabulation mapping for the Answer
     * @return Mapping of {@link Option} -> {@link Integer}
     * @exception DependencyException unsupported in case of {@link suite.model.answers.RankingAnswers}
     */
    Map<Option, Integer> getTabulation() throws DependencyException;

    /**
     * Sets the tabulation mapping of the answers
     * @param tabulation Mapping of {@link Option} -> {@link Integer}
     * @return {@link Answers}
     * @exception DependencyException unsupported in case of {@link suite.model.answers.RankingAnswers}
     */
    Answers setTabulation(Map<Option, Integer> tabulation) throws DependencyException;

    /**
     * Gets the current choice of the user
     * @return {@link Options}
     */
    Options getChoice();

    /**
     * Sets the choice for the user
     * @param choice {@link Options}
     * @return {@link Answers}
     * @throws DependencyException In case of operational errors
     */
    Answers setChoice(Options choice) throws DependencyException;

    /**
     * Sets the current choice for the user
     * @param choice stream of {@link Option}
     * @return {@link Answers}
     * @throws DependencyException In case an error is returned by setChoice(Options)
     */
    Answers setChoice(Option... choice) throws DependencyException;

    /**
     * Sets the {@link QuestionTypes} for the answer
     * @param type {@link QuestionTypes}
     * @return {@link Answers}
     */
    Answers setType(QuestionTypes type);

    /**
     * Gets the {@link QuestionTypes} for the anser
     * @return {@link QuestionTypes}
     */
    QuestionTypes getType();

    /**
     * Gets the ranking permutations for the question
     * @return Map of {@link RankingOptions} -> {@link Integer}
     * @throws DependencyException In case it is used with {@link suite.model.answers.DefaultAnswers} or operational errors
     */
    Map<RankingOptions, Integer> getRanks() throws DependencyException;

    /**
     * Sets the ranking permutations for the question
     * @param ranks Map of {@link RankingOptions} -> {@link Integer}
     * @return {@link Answers}
     * @throws DependencyException In case it is used with {@link suite.model.answers.DefaultAnswers} or operational errors
     */
    Answers setRanks(Map<RankingOptions, Integer> ranks) throws DependencyException;

}
