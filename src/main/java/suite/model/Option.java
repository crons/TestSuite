package suite.model;

import suite.model.option.OptionTypes;
import suite.util.DependencyException;
import suite.util.stream.Stream;

/**
 * <p>
 *     Interface for the option to the questions. <br>
 *         Contains the text for the response option or the
 *         entire response in case of essay questions
 * </p>
 * @author Prakash
 */

public interface Option {

    /**
     * Gets the option in String format
     * @return String
     */
    String getOption();

    /**
     * <p>
     *     Sets the String option for the Option class
     * </p>
     * @param option String option
     * @return {@link Option}
     */
    Option setOption(String option);

    /**
     * Gets the identifier for the option
     * @return String identifier
     */
    String getIdentifier();

    /**
     * Sets the string identifier for the option
     * @param identifier String identifier
     * @return {@link Option}
     */
    Option setIdentifier(String identifier);

    /**
     * Gets the option type
     * @return {@link OptionTypes}
     */
    OptionTypes getType();

    /**
     * Sets the option type
     * @param type {@link OptionTypes}
     * @return {@link Option}
     */
    Option setType(OptionTypes type);

    /**
     * Gets the rank for the option
     * @return {@link String} rank
     * @throws DependencyException Unsupported operation for {@link suite.model.option.DefaultOption}
     */
    String getRank() throws DependencyException;

    /**
     * Sets the rank for the option
     * @param rank {@link String}
     * @return {@link Option}
     * @throws DependencyException Unsupported operation for {@link suite.model.option.DefaultOption}
     */
    Option setRank(String rank) throws DependencyException;

    /**
     * Sets the order for the option
     * @param order {@link Integer} order
     * @return {@link Option}
     * @throws DependencyException Unsupported in case of {@link suite.model.option.DefaultOption}
     */
    Option setOrder(Integer order) throws DependencyException;

    /**
     * Gets the order for the option
     * @return {@link Integer}
     * @throws DependencyException Unsupported in case of {@link suite.model.option.DefaultOption}
     */
    Integer getOrder() throws  DependencyException;

}
