package suite.model.suite;

import suite.model.Option;
import suite.model.Question;
import suite.model.Suite;
import suite.model.answers.DefaultAnswers;
import suite.model.answers.EssayAnswers;
import suite.model.answers.RankingAnswers;
import suite.model.options.RankingOptions;
import suite.model.question.QuestionTypes;
import suite.util.DependencyException;

import java.util.LinkedList;
import java.util.List;

/**
 * <p>
 *     Abstraction for the Test model to specify a few extra
 *     overrides from the Survey model
 * </p>
 *
 * @author Prakash
 */
public class Test extends Survey implements Suite, java.io.Serializable {

    /**
     * Default constructor
     * @deprecated because uses a deprecated base class constructor
     */
    @Deprecated
    public Test() {
        super();
    }

    /**
     * Parametrized constructor that gets the value from bean
     * configuration
     * @param type specified {@link SuiteTypes}
     */
    public Test(SuiteTypes type) {
        super(type);
    }

    /**
     * Adds the questions to the suite
     * @param questions Stream of {@link Question}
     * @return {@link Test}
     * @throws DependencyException In case of operational errors in {@link DefaultAnswers}
     */
    @Override
    public Test addQuestion(Question... questions) throws DependencyException {
        for (Question question: questions) {
            QuestionTypes type = question.getType();
            if (type.isEssay()) {
                this.questions.put(question, new EssayAnswers(question.getType()));
            } else if (type.isShortAnswer()) {
                this.questions.put(question, new EssayAnswers(type, question.getOptions().options()));
            } else if (type.isRanking() || type.isMatching()) {
                this.questions.put(question, new RankingAnswers(type, (RankingOptions) question.getOptions()));
            } else {
                this.questions.put(question, new DefaultAnswers(type, question.getOptions().options()));
            }
        }
        return this;
    }

    @Override
    public Test clearTabulation(Question question) {
        QuestionTypes type = question.getType();
        if (type.isEssay()) {
            this.questions.replace(question, new EssayAnswers(question.getType()));
        } else if (type.isShortAnswer()) {
            this.questions.replace(question, new EssayAnswers(type, question.getOptions().options()));
        } else if (type.isRanking() || type.isMatching()) {
            this.questions.replace(question, new RankingAnswers(type, (RankingOptions) question.getOptions()));
        } else {
            this.questions.replace(question, new DefaultAnswers(type, question.getOptions().options()));
        }
        return this;
    }

    /**
     * Grades the test and returns the grade as a result
     * @return {@link String}
     * @throws DependencyException In case of operational errors
     */
    @Override
    public String grade() throws DependencyException {
        List<Boolean> correct = new LinkedList<>();
        getQuestions().forEach((question, answers) -> {
            if (!question.getType().isEssay() && answers.getChoice().getType() != QuestionTypes.NONE) {
                List<Option> options = answers.getChoice().options();
                if (question.getOptions().verify(options.toArray(new Option[options.size()]))) {
                    correct.add(true);
                } else {
                    correct.add(false);
                }
            }
        });
        return "\n\n\t[ SCORE ]\t\t" +
                correct.stream().filter(grade -> grade).count() * 10 +
                "/" + correct.size() * 10;
    }

    /**
     * <p>
     *     Override for the toString() method to get the
     *     correct answer as well
     * </p>
     * @return {@link String}
     */
    @Override
    public String toString() {
        String str = "";
        str += "Test: " + getName() + "\n\n";
        for (Question question: getQuestionsList()) {
            str += question + "\n" +
                    question.answer() + "\n\n";
        }
        return str;
    }

}
