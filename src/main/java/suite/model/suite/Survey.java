package suite.model.suite;

import suite.model.Answers;
import suite.model.Option;
import suite.model.Question;
import suite.model.Suite;
import suite.model.answers.DefaultAnswers;
import suite.model.answers.EssayAnswers;
import suite.model.answers.RankingAnswers;
import suite.model.options.RankingOptions;
import suite.model.question.QuestionTypes;
import suite.util.DependencyException;
import suite.util.LogContext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 *      Abstraction for the survey suite to provide the question
 *      and a list of options as answers.
 * </p>
 *
 * @author Prakash
 */
public class Survey implements Suite, java.io.Serializable {

    private final LogContext LOGGER = new LogContext(Survey.class);

    private String name;
    protected Map<Question, Answers> questions;
    private SuiteTypes type;

    /**
     * Default constructor for the Survey suite
     * @deprecated Use parametrized constructor
     */
    @Deprecated
    public Survey() {
        name = "Default";
        questions = new HashMap<>();
        type = SuiteTypes.SURVEY;
    }

    /**
     * Parametrized constructor that gets the values from the
     * bean configurations
     * @param type {@link SuiteTypes}
     */
    public Survey(SuiteTypes type) {
        questions = new HashMap<>();
        this.type = type;
    }

    /**
     * Gets the name for the suite
     * @return {@link String}
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Sets the name for the Survey
     * @param name Name
     * @return {@link Survey}
     */
    @Override
    public Survey setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * Gets the type for the survey
     * @return {@link SuiteTypes}
     */
    @Override
    public SuiteTypes getType() {
        return type;
    }

    /**
     * Sets the type for the survey
     * @param type {@link SuiteTypes}
     * @return {@link Survey}
     */
    @Override
    public Survey setType(SuiteTypes type) {
        this.type = type;
        return this;
    }

    /**
     * Sets the questions mapping entirely used while deserializing
     * @param questions Map of {@link Question} -> {@link Answers}
     * @return {@link Survey} object
     */
    @Override
    public Survey setQuestions(Map<Question, Answers> questions) {
        this.questions = questions;
        return this;
    }

    /**
     * Gets the possible questions as a list of prompts
     * @return list of {@link String}
     */
    @Override
    public List<String> getPossibleQuestions() {
        return getQuestionsList()
                .stream()
                .map(question -> "\n" + question.getPrompt())
                .collect(Collectors.toList());
    }

    /**
     * Clears the tabulation when the question is modified
     * @param question specified {@link Question}
     * @return {@link Survey}
     */
    @Override
    public Survey clearTabulation(Question question) {
        QuestionTypes type = question.getType();
        if (type.isEssay() || type.isShortAnswer()) {
            this.questions.replace(question, new EssayAnswers(question.getType()));
        } else if (type.isMatching() || type.isRanking()) {
            this.questions.replace(question, new RankingAnswers(type));
        } else {
            this.questions.replace(question, new DefaultAnswers(type, question.getOptions().options()));
        }
        return this;
    }

    /**
     * Gets the questions list for the survey
     * @return List of {@link Question}
     */
    @Override
    public List<Question> getQuestionsList() {
        return questions
                .keySet()
                .stream()
                .collect(Collectors.toList());
    }

    /**
     * Gets the questions for the survey
     * @return Map of {@link Question} -> {@link Answers}
     */
    @Override
    public Map<Question, Answers> getQuestions() {
        return questions;
    }

    /**
     * Adds the questions to the suite
     * @param questions Stream of {@link Question}
     * @return {@link Survey}
     * @throws DependencyException In case of operational errors in {@link DefaultAnswers}
     */
    @Override
    public Survey addQuestion(Question... questions) throws DependencyException {
        for (Question question: questions) {
            QuestionTypes type = question.getType();
            if (type.isEssay() || type.isShortAnswer()) {
                this.questions.put(question, new EssayAnswers(question.getType()));
            } else if (type.isMatching() || type.isRanking()) {
                this.questions.put(question, new RankingAnswers(type));
            } else {
                this.questions.put(question, new DefaultAnswers(type, question.getOptions().options()));
            }
        }
        return this;
    }

    /**
     * Add the answer to the specified question (can be multiple options)
     * @param question specified {@link Question}
     * @param options correct stream of {@link Option}
     * @return {@link Survey}
     * @throws DependencyException In case the question does not exist
     */
    @Override
    public Survey addAnswer(Question question, Option... options)
            throws DependencyException {
        LOGGER.addContext("Method", "addAnswer");
        if (this.questions.containsKey(question)) {
            this.questions.get(question).setChoice(options);
        } else {
            LOGGER.error(new DependencyException());
            throw new DependencyException("Question does not exist");
        }
        return this;
    }

    /**
     * Override for the equals() method
     * @param o another Object
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Survey)) return false;

        Survey survey = (Survey) o;

        if (getName() != null ? !getName().equals(survey.getName()) : survey.getName() != null) return false;
        return getQuestions() != null ? getQuestions().equals(survey.getQuestions()) : survey.getQuestions() == null;
    }

    /**
     * Override for the hashcode() function
     * @return hashcode
     */
    @Override
    public int hashCode() {
        int result = getName() != null ? getName().hashCode() : 0;
        result = 31 * result + (getQuestions() != null ? getQuestions().hashCode() : 0);
        return result;
    }

    /**
     * Tabulate the {@link Suite}
     * @return {@link String}
     */
    @Override
    public String tabulate() {
        StringBuilder stringBuilder = new StringBuilder("\n[ TABULATION ] : [ " + name + " ]\n\n");
        questions.forEach((question, answers) -> {
            stringBuilder.append(question + "\n" + answers + "\n\n");
        });
        return stringBuilder.toString();
    }

    /**
     * @throws DependencyException Since {@link Survey} can't be graded
     */
    @Override
    public String grade() throws DependencyException {
        LOGGER.addContext("Method", "grade");
        LOGGER.fatal(new DependencyException());
        throw new DependencyException("Survey can't be graded!");
    }

    /**
     * Override for the toString() method that displays the
     * question and the answers
     * @return String
     */
    @Override
    public String toString() {
        String str = "";
        str += "Survey: " + name + "\n\n";
        for (Question question: getQuestionsList()) {
            str += question + "\n\n";
        }
        return str;
    }

}
