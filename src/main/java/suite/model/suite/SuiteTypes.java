package suite.model.suite;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * <p>
 *     Enumeration class for the different types of
 *     suites used by the menus
 * </p>
 *
 * @author Prakash
 */

public enum SuiteTypes implements java.io.Serializable {

    SURVEY(true, false),
    TEST(false, true),
    NONE(false, false)
    ;

    private final boolean survey;
    private final boolean test;

    /**
     * Parametrized constructor to set the parameters
     * @param survey true if it is a survey
     * @param test true if it is a test
     */
    SuiteTypes(final boolean survey, final boolean test) {
        this.survey = survey;
        this.test = test;
    }

    /**
     * Returns whether it is a survey
     * @return {@link Boolean}
     */
    public boolean isSurvey() {
        return survey;
    }

    /**
     * @return whether it is a test
     */
    public boolean isTest() {
        return test;
    }

    /**
     * Override to get a reflection of the object
     * @return
     */
    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(
                this,
                ToStringStyle.MULTI_LINE_STYLE
        );
    }

}
