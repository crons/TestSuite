package suite.model;

import suite.model.question.QuestionTypes;
import suite.util.DependencyException;

import java.util.List;

/**
 * <p>
 *     Interface for the options for the questions that
 *     support the options. Questions like these may include
 *     ranking and multiple choice question. <br>
 *         Generally, includes a mapping of {@link Option} objects
 * </p>
 *
 * @author Prakash
 */

public interface Options {

    /**
     * <p>
     *     Sets the mapping for the option parameters (if they are
     *     already in the mapping) to a specified value. <br>
     *         Value might be <b>OPTION-TYPES</b> in case of multiple-choice
     *         questions or <b>Integers</b> in case of rankings
     * </p>
     * @param options {@link Option} stream
     * @return {@link Options}
     * @throws DependencyException If the option is not compatible to grading/ranking
     */
    Options set(Option... options)
            throws DependencyException;

    /**
     * Gets the {@link Option} list from the options
     * @return {@link Option} list
     */
    List<Option> options();

    /**
     * <p>
     *     Gets the correct {@link Options} as of the moment the
     *     call is made. <br>
     *         <ul>
     *             <li>In case of multiple-choice questions, (Default), only <b>CORRECT</b>
     *             answers are included</li>
     *             <li>In case of ranking question, the current ranking mapping is included</li>
     *         </ul>
     * </p>
     * @return {@link Options}
     * @throws DependencyException In case of streaming errors
     */
    Options correct()
            throws DependencyException;

    /**
     * <p>
     *     Checks whether the specified options are correct or not. <br>
     *         The options provided need to be gradable/rankable
     * </p>
     * @param options specified {@link Option}
     * @return boolean
     * @throws DependencyException In case the option is not gradable/rankable
     */
    boolean verify(Option... options)
            throws DependencyException;

    /**
     * <p>
     *     Checks to see whether the {@link Options} instance contains
     *     the specified {@link Option} objects
     * </p>
     * @param options {@link Option} objects
     * @return boolean
     */
    boolean contains(Option... options);

    /**
     * Gets the count of the options
     * @return size (int)
     */
    int size();

    /**
     * Gets the type of the question
     * @return {@link QuestionTypes}
     */
    QuestionTypes getType();

    /**
     * Sets the type of the question
     * @param type {@link QuestionTypes}
     * @return {@link Options}
     */
    Options setType(QuestionTypes type);

    /**
     * Clears the correct option
     * @return {@link Options}
     * @exception DependencyException Operational errors
     */
    Options clean() throws DependencyException;

    /**
     * Refreshes the correct answer for the options
     * @return {@link Options}
     * @throws DependencyException Not applicable for {@link suite.model.options.DefaultOptions}
     *                              and {@link suite.model.options.EssayOptions}
     */
    Options refresh() throws DependencyException;

}
