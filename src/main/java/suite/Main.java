package suite;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import suite.config.MenuConfig;
import suite.model.Menu;

public class Main {

    public static void main(String args[]) {
        Logger.getRootLogger().setLevel(Level.FATAL);
        ApplicationContext context = new AnnotationConfigApplicationContext(MenuConfig.class);
        Menu menu = context.getBean("MainMenu", Menu.class);
        menu.run();
    }

}
