package suite.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import suite.model.Option;
import suite.model.option.DefaultOption;
import suite.model.option.EssayOption;
import suite.model.option.OptionTypes;
import suite.model.option.RankingOption;

/**
 * <p>
 *     Configurations for some set options. <br>
 *         For example, true, false etc
 * </p>
 *
 * @author Prakash
 */

@Configuration
public class OptionConfig {

    /**
     * Bean for True option
     * @return {@link Option}
     */
    @Bean
    @Lazy
    @Scope("prototype")
    public Option True() {
        return new DefaultOption("T", "T");
    }

    /**
     * Bean for False option
     * @return {@link Option}
     */
    @Bean
    @Lazy
    @Scope("prototype")
    public Option False() {
        return new DefaultOption("F", "F");
    }

    /**
     * Bean for default option
     * @return {@link Option}
     */
    @Bean
    @Lazy
    @Scope("prototype")
    public Option Default() {
        return new DefaultOption(OptionTypes.INCORRECT);
    }

    /**
     * Bean for ranking option
     * @return {@link Option}
     */
    @Bean
    @Lazy
    @Scope("prototype")
    public Option Ranking() {
        return new RankingOption(OptionTypes.RANKINCORRECT)
                .setRank("N/A");
    }

    /**
     * Bean configuration for Matching option
     * @return {@link Option}
     */
    @Bean
    @Lazy
    @Scope("prototype")
    public Option Matching() {
        return new RankingOption(OptionTypes.MATHCINCORRECT)
                .setRank("N/A");
    }

    /**
     * Bean configuration for Essay Option (response)
     * @return {@link Option}
     */
    @Bean
    @Lazy
    @Scope("prototype")
    public Option Essay() {
        return new EssayOption(OptionTypes.ESSAY)
                .setIdentifier("N/A");
    }

    /**
     * Bean configuration for Short question (response)
     * @return {@link Option}
     */
    @Bean
    @Lazy
    @Scope("prototype")
    public Option Short() {
        return new EssayOption(OptionTypes.SHORTINCORRECT)
                .setIdentifier("N/A");
    }

}
