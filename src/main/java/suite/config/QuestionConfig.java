package suite.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import suite.model.Question;
import suite.model.question.DefaultQuestion;
import suite.model.question.EssayQuestion;
import suite.model.question.QuestionTypes;
import suite.model.question.RankingQuestion;

/**
 * <p>
 *     Configurations for the different types of
 *     questions
 * </p>
 *
 * @author Prakash
 */

@Configuration
@Import(OptionConfig.class)
public class QuestionConfig {

    @Autowired
    private OptionConfig optionConfiguration;

    /**
     * Bean configuration for a survey true/false question
     * @return {@link Question}
     */
    @Bean
    @Lazy
    @Scope("prototype")
    public Question SurveyTrueFalse() {
        return new DefaultQuestion(QuestionTypes.SURVEY_TRUE_FALSE)
                .setOptions(optionConfiguration.False(), optionConfiguration.True());
    }

    /**
     * Bean configuration for a test true/false question
     * @return {@link Question}
     */
    @Bean
    @Lazy
    @Scope("prototype")
    public Question TestTrueFalse() {
        return new DefaultQuestion(QuestionTypes.TEST_TRUE_FALSE)
                .setOptions(optionConfiguration.False(), optionConfiguration.True());
    }

    /**
     * <p>
     *     Bean configuration for a survey default question. <br>
     *         Generally, contains the multiple-choice single correct answer
     * </p>
     * @return {@link Question}
     */
    @Bean
    @Lazy
    @Scope("prototype")
    public Question SurveyDefault() {
        return new DefaultQuestion(QuestionTypes.SURVEY_DEFAULT);
    }

    /**
     * <p>
     *     Bean configuration for a test default question. <br>
     *         Generally, contains the multiple-choice single correct answer
     * </p>
     * @return {@link Question}
     */
    @Bean
    @Lazy
    @Scope("prototype")
    public Question TestDefault() {
        return new DefaultQuestion(QuestionTypes.TEST_DEFAULT);
    }

    /**
     * <p>
     *     Bean configuration for a survey ranking question
     * </p>
     * @return {@link Question}
     */
    @Bean
    @Lazy
    @Scope("prototype")
    public Question SurveyRanking() {
        return new RankingQuestion(QuestionTypes.SURVEY_RANKING);
    }

    /**
     * <p>
     *     Bean configuration for a test ranking question
     * </p>
     * @return {@link Question}
     */
    @Bean
    @Lazy
    @Scope("prototype")
    public Question TestRanking() {
        return new RankingQuestion(QuestionTypes.TEST_RANKING);
    }

    /**
     * Bean configuration for Essay question in Survey
     * @return {@link Question}
     */
    @Bean
    @Lazy
    @Scope("prototype")
    public Question SurveyEssay() {
        return new EssayQuestion(QuestionTypes.SURVEY_ESSAY);
    }

    /**
     * Bean configuration for Essay question in Test
     * @return {@link Question}
     */
    @Bean
    @Lazy
    @Scope("prototype")
    public Question TestEssay() {
        return new EssayQuestion(QuestionTypes.TEST_ESSAY);
    }

    /**
     * Bean configuration for Survey Short Answer question
     * @return {@link Question}
     */
    @Bean
    @Lazy
    @Scope("prototype")
    public Question SurveyShort() {
        return new EssayQuestion(QuestionTypes.SURVEY_SHORT);
    }

    /**
     * Bean configuration for Test Short Answer question
     * @return {@link Question}
     */
    @Bean
    @Lazy
    @Scope("prototype")
    public Question TestShort() {
        return new EssayQuestion(QuestionTypes.TEST_SHORT);
    }

    /**
     * Bean configuration for Survey Matching question
     * @return {@link Question}
     */
    @Bean
    @Lazy
    @Scope("prototype")
    public Question SurveyMatching() {
        return new RankingQuestion(QuestionTypes.SURVEY_MATCHING);
    }

    /**
     * Bean configuration for Test Matching question
     * @return {@link Question}
     */
    @Bean
    @Lazy
    @Scope("prototype")
    public Question TestMatching() {
        return new RankingQuestion(QuestionTypes.TEST_MATCHING);
    }

}
