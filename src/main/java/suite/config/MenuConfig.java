package suite.config;

import com.google.common.collect.ImmutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import suite.model.Menu;
import suite.model.menu.MainMenu;
import suite.model.menu.SurveyMenu;
import suite.model.menu.SuiteMenu;
import suite.model.menu.TestMenu;
import suite.model.suite.SuiteTypes;

/**
 * <p>
 *     Spring configurations for the different {@link Menu} items
 *     used by the TestSuite
 * </p>
 *
 * @author Prakash
 */

@Configuration
@Import({StreamConfig.class, SuiteConfig.class, QuestionConfig.class})
public class MenuConfig {

    @Autowired
    private StreamConfig stream;

    @Autowired
    private SuiteConfig suite;

    /**
     * Bean for MainMenu configuration
     * @return {@link Menu} object
     */
    @Bean
    @Lazy(false)
    @Scope("prototype")
    public Menu MainMenu() {
        return new MainMenu(
                "1. Survey\n" +
                        "2. Test\n" +
                        "3. Quit\n",
                ImmutableList.of("1", "2", "3"),
                stream.Voice()
        );
    }

    /**
     * Bean for SurveyMenu configuration which is a suite menu
     * @return {@link Menu} object
     */
    @Bean
    @Lazy(false)
    @Scope("prototype")
    public Menu SurveyMenu() {
        return new SuiteMenu(
                "1. Create New Survey\n" +
                        "2. Display Survey\n" +
                        "3. Save Survey\n" +
                        "4. Load Survey\n" +
                        "5. Add questions\n" +
                        "6. Modify an Existing Survey\n" +
                        "7. Take a Survey\n" +
                        "8. Tabulate a Survey\n" +
                        "9. Back\n" +
                        "10. Quit\n",
                ImmutableList.of("1", "2", "3", "4", "5", "6", "7", "8", "9", "10"),
                stream.Voice(),
                SuiteTypes.SURVEY
        );
    }

    /**
     * Bean for TestMenu as a SuiteMenu
     * @return {@link Menu} object
     */
    @Bean
    @Lazy(false)
    @Scope("prototype")
    public Menu TestMenu() {
        return new SuiteMenu(
                "1. Create New Test\n" +
                        "2. Display Test\n" +
                        "3. Save Test\n" +
                        "4. Load Test\n" +
                        "5. Add questions\n" +
                        "6. Modify an Existing Test\n" +
                        "7. Take a Test\n" +
                        "8. Tabulate a Test\n" +
                        "9. Grade a Test\n" +
                        "10. Back\n" +
                        "11. Quit\n",
                ImmutableList.of("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"),
                stream.Voice(),
                SuiteTypes.TEST
        );
    }

    /**
     * Bean for SurveyQuestionMenu configuration implementing SurveyMenu
     * @return {@link Menu} object
     */
    @Bean
    @Lazy(false)
    @Scope("prototype")
    public Menu SurveyQuestionMenu() {
        return new SurveyMenu(
                "1. Add a new T/F question\n" +
                        "2. Add a new multiple choice question\n" +
                        "3. Add a new essay type question\n" +
                        "4. Add a new short answer type question\n" +
                        "5. Add a new matching question\n" +
                        "6. Add a new ranking question\n" +
                        "7. Back\n",
                ImmutableList.of("1", "2", "3", "4", "5", "6", "7"),
                stream.Voice(),
                SuiteTypes.SURVEY,
                suite.Survey()
        );
    }

    /**
     * Bean for TestQuestionMenu configuration implementing TestMenu
     * @return {@link Menu} object
     */
    @Bean
    @Lazy(false)
    @Scope("prototype")
    public Menu TestQuestionMenu() {
        return new TestMenu(
                "1. Add a new T/F question\n" +
                        "2. Add a new multiple choice question\n" +
                        "3. Add a new essay question\n" +
                        "4. Add a new short answer question\n" +
                        "5. Add a new matching question\n" +
                        "6. Add a new ranking question\n" +
                        "7. Back\n",
                ImmutableList.of("1", "2", "3", "4", "5", "6", "7"),
                stream.Voice(),
                SuiteTypes.TEST,
                suite.Test()
        );
    }

}
