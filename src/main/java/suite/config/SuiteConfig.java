package suite.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import suite.model.Suite;
import suite.model.suite.SuiteTypes;
import suite.model.suite.Survey;
import suite.model.suite.Test;

/**
 * <p>
 *     Configuration file for the Suites to be used up
 *     by the menus.
 * </p>
 *
 * @author Prakash
 */

@Configuration
public class SuiteConfig {

    /**
     * <p>
     *     Returns the Survey suite as a bean for menus
     * </p>
     * @return {@link Suite} object
     */
    @Bean
    @Lazy(false)
    @Scope("prototype")
    public Suite Survey() {
        return new Survey(SuiteTypes.SURVEY);
    }

    /**
     * Returns the Test configuration suite as a bean
     * @return {@link Suite} object
     */
    @Bean
    @Lazy(false)
    @Scope("prototype")
    public Suite Test() {
        return new Test(SuiteTypes.TEST);
    }

}
