package suite.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import suite.util.stream.Stream;
import suite.util.stream.StreamTypes;

/**
 * <p>
 *     Spring configuration for the different types of {@link Stream}
 *     inputs and outputs used by TestSuite.
 * </p>
 *
 * @author Prakash
 */

@Configuration
public class StreamConfig {

    /**
     * <p>
     *     Bean configuration for console type stream. {@link StreamTypes}
     *     for both input and output are <b>CONSOLE</b>
     * </p>
     * @return {@link Stream} object
     */
    @Bean
    @Lazy(false)
    @Scope("prototype")
    public Stream Console() {
        return new Stream(
                StreamTypes.CONSOLE,
                StreamTypes.CONSOLE
        );
    }

    /**
     * <p>
     *     Bean config for the voice type stream. {@link StreamTypes} for
     *     input is <b>CONSOLE</b> and output is <b>VOICE</b>
     * </p>
     * @return {@link Stream}
     */
    @Bean
    @Lazy(false)
    @Scope("prototype")
    public Stream Voice() {
        return new Stream(
                StreamTypes.CONSOLE,
                StreamTypes.VOICE
        );
    }

}
