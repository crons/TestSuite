package suite.util;

/**
 * <p>
 *     Exception class to indicate errors in the framework. <br>
 *         <ul>
 *             <li>By default, the suppression is <b>OFF</b></li>
 *             <li>writeableStackTrace is <b>ON</b></li>
 *             <li>The exception prints the stack trace of the cause</li>
 *         </ul>
 * </p>
 * @author Prakash
 */

public class DependencyException extends RuntimeException {

    private static final long serialVersionUID = -1516235514016174665L;

    /**
     * Default constructor for the exception
     */
    public DependencyException() {
        super(null, null, false, true);
    }

    /**
     * Constructor binding a message
     * @param message Message for the exception
     */
    public DependencyException(String message) {
        super(message, null, false, true);
    }

    /**
     * Constructor binding a cause for the exception
     * @param cause Cause for the exception
     */
    public DependencyException(Throwable cause) {
        super(null, cause, false, true);
    }

    /**
     * Constructor binding a message and the cause
     * @param message Message for the exception
     * @param cause Cause for the exception
     */
    public DependencyException(String message, Throwable cause) {
        super(message, cause, false, true);
    }

}
