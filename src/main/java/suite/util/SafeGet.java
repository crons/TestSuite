package suite.util;

import org.apache.commons.lang3.StringUtils;
import suite.model.Option;
import suite.model.Question;
import suite.model.Suite;
import suite.model.question.RankingQuestion;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *     Handles the edge case senarios while user input and output. <br>
 *         Generally throws a {@link DependencyException} in case of edge case
 *         unless mentioned otherwise
 * </p>
 *
 * @author Prakash
 */

public class SafeGet {

    private static final LogContext LOGGER = new LogContext(SafeGet.class);

    /**
     * Handles the edge case for converting string to integer
     * @param input String input
     * @return converted {@link Integer} object
     * @throws DependencyException In case there is error while conversion
     */
    public static Integer integer(String input) throws DependencyException {
        LOGGER.addContext("Method", "integer");
        LOGGER.addContext("String", input);
        try {
            return Integer.parseInt(input);
        } catch (Exception e) {
            LOGGER.error(e);
            throw new DependencyException("Can't convert string to integer", e);
        }
    }

    /**
     * Handles the edge case for taking string as an input
     * @param input String input
     * @return {@link String} object
     * @throws DependencyException In case the input string is empty
     */
    public static String string(String input) throws DependencyException {
        LOGGER.addContext("Method", "string");
        LOGGER.addContext("Input", input);
        if ("".equals(input) || input.isEmpty()) {
            LOGGER.error(new DependencyException());
            throw new DependencyException("Input string is empty");
        }
        return input;
    }

    /**
     * Handles the edge case for converting the identifier to the correct option
     * and setting that correct option to the specified question
     * @param question specified {@link Question}
     * @param options String option
     * @return {@link Question}
     * @throws DependencyException In case there are operational errors while setting the correct option
     */
    public static Question questionWithCorrectOption(Question question, String... options)
            throws DependencyException {
        LOGGER.addContext("Method", "questionWithCorrectOption");
        LOGGER.addContext("Option Identifier", Arrays.deepToString(options));
        LOGGER.addContext("Question", question.toString());
        try {
            return question.setCorrectOption(options);
        } catch (DependencyException e) {
            LOGGER.error(e);
            throw new DependencyException("Can't set the correct option toi the question", e);
        }
    }

    /**
     * <p>
     *     Takes the input and tries to convert it into natural number, i.e.,
     *     1 - N
     * </p>
     * @param input String input
     * @param number Number for validation
     * @return Integer
     * @throws DependencyException In case the input is not natural number
     */
    public static Integer numberGreaterThan(int number, String input) throws DependencyException {
        LOGGER.addContext("Method", "naturalNumber");
        LOGGER.addContext("Input", input);
        try {
            Integer integer = integer(input);
            if (integer < number) {
                throw new DependencyException();
            }
            return integer;
        } catch (DependencyException e) {
            LOGGER.error(e);
            throw new DependencyException("Number entered is not natural", e);
        }
    }

    /**
     * Gets a choice from the user and returns true/false based on that
     * @param input Choice string
     * @return boolean
     * @throws DependencyException If the input is not y/Y/n/N
     */
    public static boolean choice(String input) throws DependencyException {
        LOGGER.addContext("Method", "choice");
        LOGGER.addContext("Input", input);
        if ("y".equalsIgnoreCase(input)) {
            return true;
        } else if ("n".equalsIgnoreCase(input)) {
            return false;
        } else {
            LOGGER.error(new DependencyException());
            throw new DependencyException("Invalid input");
        }
    }

    /**
     * Sets the correct {@link Option} to the {@link Question}
     * @param question specified {@link Question}
     * @param options {@link Option} stream
     * @return same {@link Question}
     * @throws DependencyException In case of operational errors
     */
    public static Question questionWithCorrectOption(Question question, Option... options)
            throws DependencyException{
        LOGGER.addContext("Method", "questionWithCorrectOption");
        LOGGER.addContext("Question", question.toString());
        LOGGER.addContext("Options", Arrays.deepToString(options));
        try {
            return question.setCorrectOption(options);
        } catch (DependencyException e) {
            LOGGER.error(e);
            throw new DependencyException("Error while setting the correct option");
        }
    }

    /**
     * Get the ranking option needed from the order
     * @param order order specified
     * @param question {@link Question}
     * @return {@link Option}
     * @throws DependencyException In case the order entered is greater than the number of options
     */
    public static Option getRankingOption(Integer order, Question question) throws DependencyException {
        LOGGER.withContext("Method", "getRankingOption")
                .withContext("Question", question.toString())
                .addContext("order", order.toString());
        try {
            if (!(question instanceof RankingQuestion) || order > question.getNumberOfChoices()) {
                throw new DependencyException();
            }
            return question.getOptions().options()
                    .stream()
                    .filter(option -> option.getOrder().equals(order))
                    .findFirst()
                    .get();
        } catch (Exception e) {
            LOGGER.error(e);
            throw new DependencyException(e);
        }
    }

    /**
     * Safely gets the rank for the {@link suite.model.question.RankingQuestion}
     * @param input String input
     * @param question specified {@link Question}
     * @return {@link String}
     * @throws DependencyException In case of operational errors
     */
    public static String rank(String input, Question question)
            throws DependencyException {
        LOGGER.addContext("Method", "rank");
        LOGGER.addContext("Question", question.toString());
        LOGGER.addContext("Input", input);
        if ((question.getType().isRanking() && !StringUtils.isNumeric(input)) ||
                !question.getPossibleChoices().contains(input)) {
            LOGGER.error(new DependencyException());
            throw new DependencyException("Rank not a number");
        }
        return string(input);
    }

    /**
     * Safely gets the rank response for the {@link suite.model.question.RankingQuestion}
     * @param input String input
     * @param question specified {@link Question}
     * @param optionsToIgnore options to ignore
     * @return {@link String}
     * @throws DependencyException In case of operational errors
     */
    public static String rankResponse(String input, Question question, List<Option> optionsToIgnore)
            throws DependencyException {
        LOGGER.addContext("Method", "rank");
        LOGGER.addContext("Question", question.toString());
        LOGGER.addContext("Input", input);
        if ((question.getType().isRanking() && !StringUtils.isNumeric(input)) ||
                !question.getPossibleSelections(optionsToIgnore).contains(input)) {
            LOGGER.error(new DependencyException());
            throw new DependencyException("Rank not a number");
        }
        return string(input);
    }

    /**
     * Safely gets the question for the suite based on the user's input
     * @param prompt {@link String} prompt for the question
     * @param suite {@link Suite} for the question
     * @return {@link Question}
     * @throws DependencyException In case of operational errors
     */
    public static Question question(String prompt, Suite suite)
            throws DependencyException {
        LOGGER.withContext("Method", "question")
                .withContext("Question", prompt)
                .withContext("Suite", suite.toString());
        try {
            return suite.getQuestionsList()
                    .stream()
                    .filter(question -> question.getPrompt().equalsIgnoreCase(prompt))
                    .findFirst()
                    .get();
        } catch (Exception e) {
            LOGGER.error(e);
            throw new DependencyException("Question not found", e);
        }
    }

    /**
     * Safely gets the {@link Option} from the {@link Question} based on the user input
     * @param identifier {@link String} option identifier
     * @param question {@link Question} for the option
     * @return {@link Option}
     * @throws DependencyException In case of operational errors
     */
    public static Option option(String identifier, Question question)
            throws DependencyException {
        LOGGER.withContext("Method", "option")
                .withContext("Identifier", identifier)
                .addContext("Question", question.toString());
        try {
            return question.getOption(identifier);
        } catch (DependencyException e) {
            LOGGER.error(e);
            throw new DependencyException("Option not found", e);
        }
    }

}
