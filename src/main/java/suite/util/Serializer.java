package suite.util;

import suite.model.Suite;
import suite.model.suite.SuiteTypes;
import suite.util.stream.Stream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *     Abstraction layer to serialize and deserialize the {@link Suite}
 * </p>
 *
 * @author Prakash
 */
public class Serializer {

    private static final LogContext LOGGER = new LogContext(Serializer.class);

    /**
     * <p>
     *     Serializes a given suite into a yaml  with the given suite
     *     name. The destination folder would be lib/
     * </p>
     * @param suite {@link Suite} to be serialized
     * @param type {@link SuiteTypes}
     * @throws DependencyException In case of serialization error
     */
    public static void serialize(final Suite suite, final SuiteTypes type) throws DependencyException {
        LOGGER.addContext("Method", "serialize");
        try {
            FileOutputStream stream = null;
            switch (type) {
                case SURVEY:
                    stream = new FileOutputStream("lib/survey/" + suite.getName() + ".ser");
                    break;
                case TEST:
                    stream = new FileOutputStream("lib/test/" + suite.getName() + ".ser");
                    break;
            }
            ObjectOutputStream outputStream = new ObjectOutputStream(stream);
            outputStream.writeObject(suite);
            outputStream.close();
            stream.close();
        } catch (Exception e) {
            LOGGER.fatal(e);
            throw new DependencyException("Error while serializing", e);
        }
    }

    /**
     * <p>
     *     Deserializes the file it gets from user input by means of getFile()
     *     and getIndex() into a {@link Suite} and returns it
     * </p>
     * @param stream {@link Stream}
     * @param type {@link SuiteTypes}
     * @return {@link Suite}
     * @throws DependencyException In case no files to load
     */
    public static Suite deserialize(final Stream stream, final SuiteTypes type)
            throws DependencyException {
        LOGGER.addContext("Method", "deserialize");
        try {
            FileInputStream file = new FileInputStream(getFile(stream, type));
            ObjectInputStream inputStream = new ObjectInputStream(file);
            Suite suite = (Suite) inputStream.readObject();
            file.close();
            inputStream.close();
            return suite;
        } catch (Exception e) {
            LOGGER.error(e);
            throw new DependencyException("Error while deserializing", e);
        }
    }

    /**
     * Gets the list of files in the <code>lib/</code> directory and
     * the file chosen by the user among them
     * @param stream {@link Stream}
     * @param type {@link SuiteTypes}
     * @return {@link File}
     */
    private static File getFile(Stream stream, SuiteTypes type) {
        LOGGER.addContext("Method", "getFile");
        try {
            File[] files = new File[10000];
            switch (type) {
                case SURVEY:
                    files = new File("lib/survey").listFiles();
                    break;
                case TEST:
                    files = new File("lib/test").listFiles();
                    break;
            }
            List<String> names = Arrays.stream(files)
                    .map(file -> file.getName())
                    .map(name -> name.split("\\.")[0])
                    .collect(Collectors.toList());
            return files[getIndex(stream, names)];
        } catch (Exception e) {
            LOGGER.error(e);
            throw new DependencyException("Fatal error while getting file", e);
        }
    }

    /**
     * <p>
     *     Gets the index of the file to be chosen from <code>lib/</code>
     *     If the user chooses the index out of range then he chooses again
     * </p>
     * @param stream {@link Stream}
     * @param fileNames list of file names
     * @return Index {@link Integer}
     */
    private static Integer getIndex(Stream stream, List<String> fileNames) {
        LOGGER.addContext("Method", "getIndex");
        if (fileNames.size() > 0) {
            stream.output("\nFiles to be loaded:\n");
            for (int i = 0; i < fileNames.size(); i++) {
                stream.output((i + 1) + ". " + fileNames.get(i));
            }
            try {
                stream.output("\nEnter the option");
                Integer choice = SafeGet.numberGreaterThan(0, stream.input());
                if (choice < 1 || choice > fileNames.size()) {
                    LOGGER.error(new DependencyException());
                    throw new DependencyException("Invalid input");
                }
                return choice - 1;
            } catch (DependencyException e) {
                stream.output("\n[ERROR] Possible choices are: [ 1 - " + fileNames.size() + " ]\n");
                return getIndex(stream, fileNames);
            }
        } else {
            stream.output("\nNo files to load\n");
            throw new DependencyException("No index to report");
        }
    }

}
