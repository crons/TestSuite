package suite.util.stream;

import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import suite.util.DependencyException;
import suite.util.LogContext;

import java.util.Scanner;

/**
 * <p>
 *     Interface for handling the input and output streams for
 *     the prompts as well as the menus
 * </p>
 *
 * @author Prakash
 */
public class Stream {

    private final LogContext LOGGER = new LogContext(Stream.class);
    private static final Scanner IN = new Scanner(System.in);

    private final static Voice VOICE = VoiceManager.getInstance().getVoice("kevin16");

    private final StreamTypes inputStream;
    private final StreamTypes outputStream;

    /**
     * <p>
     *     Default constructor that assumes that both the
     *     input and output streams are based on CONSOLE
     * </p>
     * @deprecated Better to specify the {@link StreamTypes}
     */
    @Deprecated
    public Stream() {
        inputStream = StreamTypes.CONSOLE;
        outputStream = StreamTypes.CONSOLE;
        VOICE.allocate();
    }

    /**
     * <p>
     *     Parametrized constructor to set the input and output
     *     {@link StreamTypes}
     * </p>
     * @param inputStream input {@link StreamTypes}
     * @param outputStream output {@link StreamTypes}
     * @see StreamTypes for more details
     */
    public Stream(StreamTypes inputStream, StreamTypes outputStream) {
        this.inputStream = inputStream;
        this.outputStream = outputStream;
        VOICE.allocate();
    }

    /**
     * <p>
     *     Inputs the stream based on the input stream types parameter
     *     and returns the string after that
     * </p>
     * @return input as a {@link String}
     * @throws DependencyException In case the input stream is invalid
     */
    public String input() throws DependencyException {
        LOGGER.addContext("Method", "input");
        if (inputStream.isConsole()) {
            return IN.nextLine();
        } else {
            LOGGER.error(new DependencyException());
            throw new DependencyException("Input Stream option invalid");
        }
    }

    /**
     * <p>
     *     Outputs the specified string based on the output stream type
     *     parameter (can be CONSOLE or VOICE for example)
     * </p>
     * @param output specified output
     * @throws DependencyException In case output stream is invalid
     */
    public void output(String output) throws DependencyException {
        LOGGER.addContext("Method", "output");
        switch (outputStream) {
            case VOICE:
            case CONSOLE:
                System.out.println(output);
                break;
            default:
                LOGGER.fatal(new DependencyException());
                throw new DependencyException("Output Stream option invalid");
        }
    }

    /**
     * <p>
     *     Outputs the given text in the console format or the
     *     voice type
     * </p>
     * @param output specified output
     * @throws DependencyException Should never be used
     */
    public void display(String output) throws DependencyException {
        LOGGER.addContext("Method", "display");
        switch (outputStream) {
            case VOICE:
                VOICE.speak(output);
                break;
            case CONSOLE:
                System.out.println(output);
                break;
            default:
                LOGGER.fatal(new DependencyException());
                throw new DependencyException();
        }
    }

    /**
     * Override for the equals() method
     * @param o another Object
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Stream)) return false;

        Stream stream = (Stream) o;

        if (inputStream != stream.inputStream) return false;
        return outputStream == stream.outputStream;
    }

    /**
     * Override for the hashcode() method
     * @return int hashcode
     */
    @Override
    public int hashCode() {
        int result = inputStream != null ? inputStream.hashCode() : 0;
        result = 31 * result + (outputStream != null ? outputStream.hashCode() : 0);
        return result;
    }

    /**
     * <p>
     *     Override to get Reflective multiline String representation
     *     for the object
     * </p>
     * @return String
     */
    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(
                this,
                ToStringStyle.MULTI_LINE_STYLE
        );
    }

}
