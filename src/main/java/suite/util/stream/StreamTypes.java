package suite.util.stream;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * <p>
 *     Enumeration class to configure different types of streaming
 *     (both input and output) available.
 * </p>
 *
 * @author Prakash
 */

public enum StreamTypes {

    CONSOLE(true, false),
    VOICE(false, true)
    ;

    private final boolean console;
    private final boolean voice;

    /**
     * Parametrized constructor for classifying parameters of stream
     * @param console whether it is console
     * @param voice whether it is voice
     */
    StreamTypes(final boolean console, final boolean voice) {
        this.console = console;
        this.voice = voice;
    }

    /**
     * Returns whether the stream is console or not
     * @return boolean
     */
    public boolean isConsole() {
        return console;
    }

    /**
     * Returns whether the stream is a voice or not
     * @return boolean
     */
    public boolean isVoice() {
        return voice;
    }

    /**
     * Override for toString() to just get the reflection of class
     * @return String
     */
    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(
                this,
                ToStringStyle.MULTI_LINE_STYLE
        );
    }

}
