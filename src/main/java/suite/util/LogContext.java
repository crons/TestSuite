package suite.util;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import java.beans.Transient;

/**
 * <p>
 *      Abstraction over log4j logging to include extra stuff
 *      while logging in case of exceptions
 * </p>
 *
 * @author Prakash
 */
public class LogContext implements java.io.Serializable {

    private StringBuilder context;
    private transient final Logger LOGGER;

    /**
     * Default constructor
     */
    public LogContext() {
        context = new StringBuilder();
        LOGGER = Logger.getLogger(LogContext.class);
    }

    /**
     * Sets the logger to the specified class
     * @param clazz Class name specified
     */
    public LogContext(Class clazz) {
        context = new StringBuilder();
        context.append("\n\n[ Class ]  " + clazz.getName());
        LOGGER = Logger.getLogger(clazz);
    }

    /**
     * Adds the context to the logger in the form of [ key] value
     * @param key String
     * @param value String
     */
    public void addContext(String key, String value) {
        context.append("\n");
        context.append("[ " + key + " ]  ");
        context.append(value);
    }

    /**
     * Returns the object after adding the context
     * @param key String
     * @param value String
     * @return {@link LogContext}
     */
    public LogContext withContext(String key, String value) {
        context.append("\n");
        context.append("[ " + key + " ]  ");
        context.append(value);
        return this;
    }

    /**
     * Getter method for the context
     * @return StringBuilder
     */
    public StringBuilder getContext() {
        return context;
    }

    /**
     * Logs the errors/exceptions
     * @param cause Throwable
     */
    public void error(Throwable cause) {
        BasicConfigurator.configure();
        if (cause != null) {
            context.append("\n[ -- ERROR -- ]  " + cause.toString());
        }
        context.append("\n");
        LOGGER.error(context.toString(), cause);
    }

    public void fatal(Throwable cause) {
        BasicConfigurator.configure();
        if (cause != null) {
            context.append("\n[ -- FATAL -- ]  " + cause.toString());
        }
        context.append("\n");
        LOGGER.fatal(context.toString(), cause);
    }

}
