# TestSuite
## Prasanna Prakash
### Student ID: 13617053
prakash@drexel.edu
---

## How to run
Running the program is based on the gradle script

### ./gradlew or ./gradlew.bat (Windows)
To install the cradle runtime dependencies and run the following script commands.

> This process only needs to be completed once

### gradle clean build
To clean the directory and build the object files. Also runs the tests.

### gradle run
To run the main program. The program runs in the following format.

```
Enter the option
<=========----> 75% EXECUTING

File Loaded!


1. Create New Test
2. Display Test
3. Save Test
4. Load Test
5. Forward
6. Back
7. Quit


Enter your option
<=========----> 75% EXECUTING
```

This is an easier way to run on command line but you can also run the Main file located at ./src/main/java/suite from *IntelliJ*

---

## Important

### For Part 4
* All the Free TTS jar files are already added there as a dependency. You will just need to **build and run** the program. If you want, the necessary files are in *lib/util*
* I have made the *Tabulation* and *Display* voice output. I was confused about the tabulation part since it is also a form of displaying the survey/test.
* If you want a brief of what I chaged you can find them in **src/main/java/stream** package
 
### For Part 3
* Modifying in case of tests include modifying the answers and the pre-existing answer is cleared for the question. So, you will have to enter the correct answers for a question as you would when you are creating it.
* When you are modifying ranking/matching question options, you are expected to set the correct options afterwards, the suite does not automatically do that
* The number of responses for short answer can't exceed the total number of answers.
* Modifying a question clears its tabulation.
* Grade grades the latest iteration of the survey/test you took (for example, if you take the survey 2 times, since it is serialized after that, only the responses of the second iteration are available to the grade() for grading)
* If you load a survey/test and start adding further questions then the tabulation for the pre-existing questions remnain the same as they were (they are not cleared)
* For the short answer type questions , in case of tests, the correct answers are available in the tabulation (initial value 0). This made sense to me.
* For the essay question, while tabulating, the repeated responses don't have multiple entries in the table. I tried to implement it in the same way (having frequencies) as I am tabulating others, but the istruction just required to print the essay responses while tabulating
* For the ranking/matching question the tabulation is presented in the following format with the matching options printed with a seperation of '-' and then the frequency after arrow. Ranking options are printed in order and then the frequency
```
[ TABULATION ] : [ rank ]
    
[RANKING]
Q: Rank
    
		A) opt1
		B) opt2
    
    
[ Option ] -> [ Frequency ]
    
	[ opt1 ]	->	1
	[ opt2 ]
	
	[ opt2 ]	->	1
	[ opt1 ]
	
    
[MATCHING]
Q: Match
    
		lhs1		 | 		 rhs1
		lhs2		 | 		 rhs2
    
    
[ Option ] -> [ Frequency ]
    
	[ lhs1 - rhs1 ]	->	1
	[ lhs2 - rhs2 ]
    
	[ lhs1 - rhs2 ]	->	1
	[ lhs2 - rhs1 ]
	   
```
* In case of Tests, it makes sense to have the correct options for matching/ranking initially (with freq 0) in the tabulation. Also, grading over here also means to grade the latest attempt.
* The matching question will guide you thorugh taking it, but in case of ranking you will enter the corresponding rank to the option displayed. Any erros in input is handled and it would prompt you the proper choices.

### For Part 2
* I have used *Spring MVC Framework* for the factory model of certain classes instead of a “is-a” relationship
* The menus might have some extra features than the one described in the assignment guidelines. Also, the layout of displaying the questions might be a little different, since for some of the questions I had to improvise. (They display all the parts though)
* For the EssayQuestion no options are being taken, the answers would be taken in the future though (This is since the essays are kind of not graded so taking options isn’t necessary)
* Some files have been provided in *lib/* to load in the program
* Assignment guidelines had two /Add a new multiple choice question/. I thought this was a typo and included a matching option instead.
* The single option: /Add a new multiple choice question/ can handle both the single correct and multiple choices if needed.
* For matching question in the question part the matching options rows might not be arranged in the same order as you entered it. This does not matter because the columns of a particular row does not change.
* The program has been tested for most of the edge cases and it should not break, but go easy on it though, thanks!


